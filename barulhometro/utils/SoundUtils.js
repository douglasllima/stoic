
// const fs = require('fs');

class SoundUtils{
    static calculaLeq(spl){

        var valor = [], freq = [], freqAcum = [], percFreq = [], prev, Leq = 0, count = -1, L10 = 0, L90 = 0, iL10 = 0, iL90 = 0;
        spl.sort(function(a,b){
            return b - a;
        });
        for ( var i = 0; i < spl.length; i++ ) {        
            if ( spl[i] !== prev ) {
                if (i == 0){
                    freqAcum.push(0);
                    percFreq.push(0);
                } else {
                    var percentual = freqAcum[freqAcum.length-1] / spl.length;
                    percFreq[percFreq.length-1] = percentual;
                    if (percentual <= 0.1){
                        L10 = valor[valor.length - 1];
                        iL10 = count;
                    } else if (percentual <= 0.9) {
                        L90 = valor[valor.length - 1];
                        iL90 = count;
                    }
                    freqAcum.push(freqAcum[freqAcum.length - 1]);
                    percFreq.push(1);
                }
                count++;
                valor.push(spl[i]);
                freq.push(1); 
            } else {
                freq[freq.length-1]++;
            }     
            freqAcum[freqAcum.length-1]++;        
            prev = spl[i];
        }
    
        if (Math.abs(percFreq[iL10+1] - 0.1) <  Math.abs(percFreq[iL10] - 0.1))
        {
            L10 = valor[iL10+1];
        } else {
            L10 = valor[iL10];
        }

        if (Math.abs(percFreq[iL90+1] - 0.9) < Math.abs(percFreq[iL90] - 0.9))
        {
            L90 = valor[iL90+1];
        } else {
            L90 = valor[iL90];
        }
    
        // for (var k = 0; k < freq.length; k++){
        //     fs.appendFile('./result.csv',valor[k] + "\t" + freq[k] + "\t" + freqAcum[k] + "\t" + percFreq[k] + "\t" + L10 + "\t" + L90  + '\n',function(err){
        //         if (err) console.error(err);
        //     })
        //     // console.log(valor[k] + "\t" + freq[k] + "\t" + freqAcum[k] + "\t" + percFreq[k] + "\t" + L10 + "\t" + L90);
        // }
    
        Leq = Math.round(0.01 * Math.pow(Number(L10)-Number(L90),2) + 0.5*(Number(L10) + Number(L90)));

        // fs.appendFile('./result.csv','Leq\t' + Leq + '\n', function(err){
        //     if (err) console.error(err);
        // })
        // console.log("Leq: " +  Leq);
    
        return Leq;
    
    }
}
module.exports = SoundUtils;