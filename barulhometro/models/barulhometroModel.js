'use strict'

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var barulhometroDeviceSchema = new Schema({
    recvTime :  Date,
    attrName :  String,
    attrType :  String,
    attrValue : String
});
module.exports = mongoose.model('barulhometro', barulhometroDeviceSchema,'sth_/_Barulhometro001_Barulhometro');
