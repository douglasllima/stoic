
function queryString(parameter) {  
              var loc = location.search.substring(1, location.search.length);   
              var param_value = false;   
              var params = loc.split("&");   
              for (i=0; i<params.length;i++) {   
                  param_name = params[i].substring(0,params[i].indexOf('='));   
                  if (param_name == parameter) {                                          
                      param_value = params[i].substring(params[i].indexOf('=')+1)   
                  }   
              }   
              if (param_value) {   
                  return param_value;   
              }   
              else {   
                  return undefined;   
              }   
        }

var variavel = queryString("valor");

// if(variavel == undefined || variavel != "corneta"){
// 	window.location = "http://www.barulhometro.com.br";
// }

var painel = 0;

/**
 * Evento ao carregar a página.
 */
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
	$('#dia').dateDropper();
	$('#dia').dateDropper({ minDate: 0 });
	$("#mapeamento-ruido").click(function(){
		alert("Lembrete: Planejar estrutura de mapeamento de ruído na cidade!");
	});

	$(document).on('click','.navbar-collapse.in',function(e) {
		if( $(e.target).is('a') && $(e.target).attr('class') != 'dropdown-toggle' ) {
			$(this).collapse('hide');
			$(".navbar-brand").css({ "display": "block" });
		}
	});	
	createWebsocket();
});

/**
 * Evento ao redimensionar a página.
 */
$(window).resize(function(){
	$('.barra').remove();
	$("#chart_div").css({ width: '100%', height: 'calc(100% - 180px)' });
	dashboard.draw(dataGrafico);
});

/**
 * Leva a página para o tópico selecionado.
 * @param area Tópico selecionado (ID).
 * @param n Margem de erro.
 */
function menu(area, n){
	$('html, body').animate({
		scrollTop: ($("#" + area).offset().top - n)
	}, 500);
}

/**
 * Invoca o painel de informações.
 * @param page Elemento a ser invocado.
 * @param lado Lado que o elemento será invocado.
 */
function pageDemoRuidoAgora(page, lado){
	if(lado == 'left'){
		if($(window).width() < 768){
			$(page).css({ display: 'block', width: '100%' });
		} else {
			$(page).css({ display: 'block', right: 'calc(50% + 100px)' });
		}
	} else {
		if($(window).width() < 768){
			$(page).css({ display: 'block', width: '100%' });
		} else {
			$(page).css({ display: 'block', left: 'calc(50% + 100px)' });
		}
	}	
	$(page).animate({ 'opacity': '1', 'top': '0px' }, 250);
}

/**
 * Fechamento do elemento invocado.
 * @param page Elemento.
 */
function closePageDemo(page){	
	$(page).animate({ 'opacity': '0', 'top': '20px' }, 250, function(){ $(page).css({ 'display': 'none' }); });
}

/**
 * Animação das opções do botão período.
 */
function menuPeriodo(){
	if($(".menu-periodo").css('opacity') == 0){
		$(".menu-periodo").css({ "display": "inline" });
		$(".menu-periodo").animate({ "display": "inline", "opacity": "1", "margin-top": "20px" }, 100);
		$(".menu-periodo").focus();
	} else {
		menuPeriodoFocusOut();
	}
}

/**
 * Retira o foco das opções do botão período.
 */
function menuPeriodoFocusOut(){
	$(".menu-periodo").animate({ "opacity": "0", "margin-top": "0px", "display": "none" }, 150, "linear", function(){
		$(".menu-periodo").css({ "display": "none" });
	});
}

/**
 * Muda o texto e ícone do botão período pela opção selecionada.
 * @param periodo Período escolhido.
 */
function periodoGrafico(periodo){
	$("#title-grafico-periodo-label").text(periodo);
	if(periodo == "Manhã") periodo = "Manha";
	$(".grafico-periodo-label").css({ 
		"background-image": "url('imgs/periodos/" + periodo + ".png')",
		"backgroind-size": "90% bottom",
		"background-repeat": "no-repeat"
	});	
}