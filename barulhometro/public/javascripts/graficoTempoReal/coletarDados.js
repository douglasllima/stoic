var wsocket;  // Objeto WebSocket
var dado = 0; // Dado do painel
var valDados; // ID do temporizador
var np = '0'; // Número do painel

function createWebsocket(){

	$("#barrasInfo").html("Conectando ao painel...");
	$(".infoBarrasConexao").html("Conectando ao painel...");
	//wsocket = new WebSocket("wss://barulhometro.ipt.br/wsbarulhometro/");
	wsocket = new WebSocket("ws://"+ location.hostname + ":8002");
	
	wsocket.onopen = function(){
		//setTimeout(function(){ wsocket.close(); }, 3000);
		$("#barrasInfo").html("Conectado!");
		$(".infoBarrasConexao").html("Conectado!");
		
		valDados = setTimeout(function(){
			$("#barrasInfo").html("Aguardando dados...");
			$(".infoBarrasConexao").html("Aguardando dados...");
		}, 1500);
	};

	wsocket.onmessage = function(message){
		if(dado == 0){
			/*if($(window).width() < 768){
				$(".banner").css({ 'height': 'calc(100% - 150px)' });
			} else {
				$(".banner").css({ 'height': 'calc(100% - 100px)' });
			}*/
			$('.infoBarrasConexao').css({ 'opacity':'0' });
			$("#barrasonline").css({ 'height': '100px', 'padding-top': '10px' });
		}
		dados = message.data.split("\t");
		dado = dados[2];
		clearInterval(valDados);
		if(dados[0] == np){
			if(dados[0] == '1'){
				$("#botaoLocalLargoDaBatata").css({ "display": "inline" });
				$("#botaoLocalParqueIbirapuera").css({ "display": "inline" });
				$("#barrasInfo").html("Dados do Largo da Batata");
				$("#dadoAtualDBA").attr('data-original-title', 'Último valor capturado do Largo da Batata');
			} else if(dados[0] == '2') {
				$("#botaoLocalParqueIbirapuera").css({ "display": "inline" });
				$("#barrasInfo").html("Dados do Parque Ibirapuera");
				$("#dadoAtualDBA").attr('data-original-title', 'Último valor capturado do Parque Ibirapuera');
			}
		} else {
			if(np == '0'){
				np = dados[0];
			}
			dado = 0;
		}
	};

	wsocket.onclose = function(evt){
		$("#barrasInfo").html("Desconectado!");
		$(".infoBarrasConexao").html("Desconectado!");
		$('.infoBarrasConexao').css({ 'opacity':'1' });
		console.log("Cod. erro: " + evt.code + ", Reason: " + evt.reason);
		$("#barrasonline").css({ 'height': '0px', 'padding-top': '0px' });
		dado = 0;
		if($(window).width() < 768){
			$(".banner").css({ 'height': 'calc(100% - 50px)' });
		} else {
			$(".banner").css({ 'height': '100%' });
		}
		setTimeout(function(){
			createWebsocket();
		}, 5000);		
	};
}

function trocarConexao(numberPainel){
	np = numberPainel;
}
