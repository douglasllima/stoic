var canvas = document.getElementById("cBarrasRT");
canvas.width = window.innerWidth - 17;
canvas.height = 50;

var requestID;										// ID das atualizações do canvas.
var lastRectangle;									// Retângulo de pause.
var ctx 				= canvas.getContext('2d');	// Canvas.
var rectangles 			= [];						// Retângulos.
var altura				= 0.58; 					// Altura para cálculo da posição e tamanho dos retângulos e labels.
var parar 				= true;						// Interrupção das atualizações.


/**
 * Calculos de cor, posição e valor para cada retângulo.
 */
function calculation(){
	
	// Calcula o valor da cor em RGB.
	if(dado < 44){
		cor = "rgb(0, 255, 0)";
	} else if(dado < 50){
		cor = "rgb(" + Math.round((dado-44)/(50-44)*255) + ", 255, 0)";
	} else if(dado < 100){
		cor = "rgb(255, " + Math.round((100-dado)/(100-50) *255) + ", 27)";
	} else {
		cor = "rgb(255, 0, 27)";
	}

	// Criação e cálculo do posicionamento de cada retângulo.
	if(rectangles.length){
		if(rectangles[rectangles.length - 1].x < (window.innerWidth - 43)){
			$("#dadoAtual").html(dado + " dBA");
			rectangles.push({
				dado: dado,
				x: window.innerWidth,
				y: Math.round(canvas.height - ((dado-39) * altura)),
				width: 40,
				height: Math.round((dado-39) * altura),
				color: cor,
				text: dado,
				speedX: 156, //Math.round(60 * 2.6)
				font: "bold 14px Myriad Pro",
				textColor: 'rgb(255, 255, 255)',
			});
		}
		if(rectangles.length > ((window.innerWidth+40) / 40)){
			rectangles.shift();
		}
	} else {
		$("#dadoAtual").html(dado + " dBA");
		rectangles.push({
			dado: dado,
			x: window.innerWidth,
			y: Math.round(canvas.height - ((dado-39) * altura)),
			width: 40,
			height: Math.round((dado-39) * altura),
			color: cor,
			text: dado,
			speedX: 156, //Math.round(60 * 2.6)
			font: "bold 14px Myriad Pro",
			textColor: 'rgb(255, 255, 255)',
		});		
	}	

	// Verificação de valor mínimo do dado.
	if(rectangles[rectangles.length - 1].dado < 40){
		rectangles.pop();
	}
}

/**
 * Atualização do canvas, que inclui a atualização do posicionamento dos retângulos e labels.
 */
function update() {
	calculation();
	ctx.save();
	ctx.clearRect(0, 0, canvas.width, canvas.height);

	var currentAnimationTime = Date.now();
	var animationTimeDelta = (currentAnimationTime - (this.lastAnimationTime || Date.now())) / 1000;
	lastAnimationTime = currentAnimationTime;

	// Atualização do posicionamento de cada retângulo.
	rectangles.forEach(function(rectangle){
		ctx.fillStyle = rectangle.color;
		ctx.fillRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);		
		rectangle.x -= 3.0; //Math.round(rectangle.speedX * animationTimeDelta);
		ctx.textAlign = "center";
		ctx.fillStyle = rectangle.textColor;
		ctx.font = rectangle.font;
		ctx.fillText(rectangle.text, rectangle.x + 20, rectangle.y - 5);	
	});

	requestID = requestAnimationFrame(update);

	// Condição de interrupção da atualização.
	if(!parar){
		$("#dadoAtual").html("Pause");
		if(lastRectangle.x < window.innerWidth - 60){			

			// Encerra as atualizações.
			window.cancelAnimationFrame(requestID);

			// Desbloqueia a ação de "click" no botão play/pause para o usuário.
			$("#barrasPause").css("pointer-events", "auto");

			// Remove o último retângulo.
			// (Após entrar nesta condição é gerado um retângulo após o "Pause", que não será utilizado).			
			rectangles.pop();
		}
	}
}

/**
 * Interrompe as atualizações do canvas.
 */
function barrasPause(){

	// Condição que verifica se as atualizações estão interrompidas.
	if(parar && requestID){	
		// Bloqueia a ação de "click" no botão play/pause para o usuário.
		$("#barrasPause").css("pointer-events", "none");
		// Troca de ícone de pause para play.
		$('#barrasPause').addClass("glyphicon-play");
		$('#barrasPause').removeClass("glyphicon-pause");
		$('#barrasPause').attr("data-original-title", "Continuar");
		
		// Insere o retângulo de pause.
		rectangles.push({
			dado: rectangles[rectangles.length - 1].dado,
			x: rectangles[rectangles.length - 1].x + 45,
			y: Math.round(canvas.height - ((rectangles[rectangles.length - 1].dado-39) * altura)),
			width: 40,
			text: "Pause",
			height: Math.round((rectangles[rectangles.length - 1].dado - 39) * altura),
			color: "rgb(255, 255, 255)",
			speedX: 156 //Math.round(60 * 2.6)
		});

		// Armazena na memória o retângulo de pause.		
		lastRectangle = rectangles[rectangles.length - 1];
		// Altera a variável de interrupção das atualizações.
		parar = false;
	} else {
		// Troca de ícone de play para pause.
		$('#barrasPause').removeClass("glyphicon-play");
		$('#barrasPause').addClass("glyphicon-pause");
		$('#barrasPause').attr("data-original-title", "Pausar");
		// Altera a varável de interrupção das atualizações.
		parar = true;
		// Atualização do canvas.
		update();
		// Reposiciona o retângulo posterior ao "pause".
		rectangles[rectangles.length - 1].x -= 18;
	}
}

/**
 * 
 */
function barrasFullScreen(){
	if($('#barrasFullScreen').hasClass('glyphicon-resize-full')){

		// Condição do tamanho do canvas e posição dos botões.
		if($(window).width() < 768){		
			$("#barrasonline").css({ 'height': 'calc(100% - 50px)' });
			canvas.height = window.innerHeight - 100;
		} else {			
			$("#barrasonline").css({ 'height': '100%' });
			canvas.height = window.innerHeight - 50;
		}
		
		$("#barrasFullScreen").removeClass('glyphicon-resize-full');
		$("#barrasFullScreen").addClass('glyphicon-resize-small');		
		$('#barrasFullScreen').attr("data-original-title", "Reduzir visualização");
		$("#dadoAtual").css({ 'font-size': '40px' });
		altura = canvas.height / 65;
	} else {
		$("#barrasonline").css({ 'height': '100px' });
		$("#barrasFullScreen").addClass('glyphicon-resize-full');
		$("#barrasFullScreen").removeClass('glyphicon-resize-small');
		altura = 0.58;
		$('#barrasFullScreen').attr("data-original-title", "Aumentar visualização");
		$("#dadoAtual").css({ 'font-size': '25px' });
		canvas.height = 50;
	}
	reorganize();
	update();
	window.cancelAnimationFrame(requestID);
}

/**
 * Alteração de tamanho e posição do canvas e dos botões ao redimensionar a tela. 
 */
function resize(){

	// Redimensiona o canvas para a largura da tela. 
	canvas.width = window.innerWidth;

	if($('#barrasFullScreen').hasClass('glyphicon-resize-small')){
		if($(window).width() < 768) {
			$("#barrasonline").css({ 'height': 'calc(100% - 50px)' });
			$('html, body').animate({
				scrollTop: $("#barrasonline").offset().top - 50
			}, 0);
			canvas.height = window.innerHeight - 100;
		} else {
			$("#barrasonline").css({ 'height': '100%' });
			$('html, body').animate({
				scrollTop: $("#barrasonline").offset().top
			}, 0);			
			canvas.height = window.innerHeight - 50;
		}
		// Altera o valor da altura para os cálculos.
		altura = canvas.height / 65;
		// Atualização dos cálculos.
		reorganize();
	}
}

/**
 * Atualiza os cálculos de posição e tamanho de todos os retângulos e labels.
 */
function reorganize(){
	rectangles.forEach(function(rec){
		rec.y = Math.round(canvas.height - ((rec.dado - 39) * altura));
		rec.height = Math.round((rec.dado - 39) * altura);
	});
}

/**
 * Chamada do método de atualização para inicio do ciclo.
 */
update();