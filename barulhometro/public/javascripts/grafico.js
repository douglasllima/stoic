/**
 * Gráfico Dados - Google Chart
 */

google.charts.load('current', {'packages':['corechart', 'controls'], 'language': 'pt'});
google.charts.setOnLoadCallback(drawDashboard);

var dashboard;
var dataGrafico;
var dateStart 		= new Date();
var dateEnd 		= new Date();
var dadosHeight;


//Create the XHR object.
function createCORSRequest(method, url) {
  var xhr = new XMLHttpRequest();
  if ("withCredentials" in xhr) {
    // XHR for Chrome/Firefox/Opera/Safari.
    xhr.open(method, url, true);
  } else if (typeof XDomainRequest != "undefined") {
    // XDomainRequest for IE.
    xhr = new XDomainRequest();
    xhr.open(method, url);
  } else {
    // CORS not supported.
    xhr = null;
  }
  return xhr;
}


/**
 * Define as opções do gráfico e o desenha.
 */
function drawDashboard() {

	dashboard = new google.visualization.Dashboard(document.getElementById('dashboard_div'));	

	var chart = new google.visualization.ChartWrapper({
		'chartType' : 'ColumnChart',
		'containerId' : 'chart_div',			
		'options' : {
			'chartArea': {'height': '90%', 'width': '90%'},
			'hAxis': {
				format: "HH:mm"
			},
			'vAxis': { 
				format: "## dB",
				'gridlines': { 
					'count': 11
				},				
				'viewWindow': {
					'min': 40,
					'max': 100
				}				
			},
			'legend': {'position': 'none'},
			/*'bar': {
				'groupWidth': '85%' 
			}*/
		}
	});
	dashboard.bind(controlDashboard(dateStart, dateEnd), chart);
}

/**
 * Define as configurações do filtro do gráfico.
 * @param dateStart Data de inicio do filtro.
 * @param dateEnd Data de encerramento do filtro.
 * @returns Objeto com as configurações do filtro.
 */
function controlDashboard(dateStart, dateEnd){
	var control = null;
	if($(window).width() > 768){
		$("#filter_div").css({ 'display': 'block' });
		$("#filter_mobile_div").css({ 'display': 'none' });
		control = new google.visualization.ControlWrapper({
			'controlType' : 'ChartRangeFilter',
			'containerId' : 'filter_div',			
			'options' : {
				'filterColumnIndex' : 0,
				'ui': {
					'chartType': 'AreaChart',
					'chartOptions': {
						'colors' : ['#000000'],
						'chartArea': {'width': '90%', 'height': '100%'},
						//'hAxis': {'baselineColor': 'none', format: "dd/MM/yyyy" },
						'vAxis': {'viewWindow': {'min': 40, 'max': 100}},
						'curveType': 'function'
					}
				},
			},
			'minRangeSize': dateEnd.getMilliseconds() - dateStart.getMilliseconds(),			
			'state': {
				'range': {
					'start': dateStart,
					'end': dateEnd
				}
			}
		});
	} else {
		$("#filter_div").css({ 'display': 'none' });
		$("#filter_mobile_div").css({ 'display': 'inline' });
		control = new google.visualization.ControlWrapper({
			'controlType': 'DateRangeFilter',
			'containerId': 'filter_mobile_div',
			'options': {
				'filterColumnIndex': 0,
				'ui': {
					'chartType': 'AreaChart',
					'label': '',
					'cssClass': 'google-visualization-controls-theme-plus',
					'step': 'minutes',
					'format': {'pattern': "HH:mm"}
				},
			},
			'state': {
				'lowValue': dateStart,
				'highValue': dateEnd
			}
		});
	}
	return control;
}

/**
 * Coleta dados do servidor via JSON.
 * @returns Dados para inserção no gráfico.
 */
function coletaDadosGrafico(){
	// Desabilita a visibilidade do gráfico e mostra mensagem de carregamento.
	desableEnableMenuGraph("0", "url('imgs/loading/l.gif') no-repeat center center ", true, "#999", "#eee");

	try{
		var dataGrafico = new google.visualization.DataTable();	
		var dia = $("#dia").val();
		var periodo = $("#title-grafico-periodo-label").text();
		if(periodo == "Manhã"){
			periodo = "Manha";
		}
		
		$.ajax({
			type: 'GET',
			url: "http://localhost:3002/Consulta?data=" + dia + "&periodo=" + periodo + "&painel=" + painel,
			//url: "http://barulhometro.org.br/Consulta?data=" + dia + "&periodo=" + periodo + "&painel=" + painel,
			dataType: 'jsonp',
			jsonp:'barulhometroCallback',
			success: function(data, status){
				// Criando colunas com seu determinado tipo.
				dataGrafico.addColumn('datetime', 'Data');
				dataGrafico.addColumn('number', 'Nível Sonoro');
				dataGrafico.addColumn({type: 'string', role: 'style'});
				// 
				if(data[0] != null){
					dia = dia.split("/");
					dateStart = new Date(dia[2], parseInt(dia[1])-1, dia[0], new Date(data[0].data).getHours(), new Date(data[0].data).getMinutes());
					dateEnd = new Date(dia[2], parseInt(dia[1])-1, dia[0], new Date(data[0].data).getHours()+1, new Date(data[0].data).getMinutes());
					desableEnableMenuGraph("1", "none", false, "#000", "#fff");
				} else {
					desableEnableMenuGraph("0", "url('imgs/loading/notfound.png') no-repeat center center", false, "#000", "#fff");
					if($(window).width() < 768){
						$("#dashboard_div").css({ "background-size": "100% auto" });
					}
				}
				// ForEach dos dados recebidos.
				$.each(data, function(i, item){
					if(item.nivelSonoro < 44){
						cor = "rgb(0, 255, 0)";
					} else if(item.nivelSonoro < 50){
						cor = "rgb(" + Math.round((item.nivelSonoro-44)/(50-44)*255) + ", 255, 0)";
					} else if(item.nivelSonoro < 100){
						cor = "rgb(255, " + Math.round((100-item.nivelSonoro)/(100-50) *255) + ", 27)";
					} else {
						cor = "rgb(255, 0, 27)";
					}
					dataGrafico.addRow([new Date(item.data), Math.floor(item.nivelSonoro), 'color: ' + cor]);
				});

				drawDashboard();
				dashboard.draw(dataGrafico);
			},
			error: function() {
				console.log("Erro durante a consulta!");
				desableEnableMenuGraph("0", "url('imgs/loading/error.png') no-repeat center center", false, "#000", "#fff");
			}			
		});
		
	} catch (e) {
		console.log("Erro durante a consulta!");
		desableEnableMenuGraph("0", "url('imgs/loading/error.png') no-repeat center center", false, "#000", "#fff");
	}

	return dataGrafico;
}

/**
 * Opções de visibilidade do menu e do gráfico.
 * @param opacity 		Nível de opacidade do gráfico.
 * @param background 	Imagem de mensagem.
 * @param disabled 		Condição para habilitar/desabilitar os botões.
 * @param colorText 	Cor do texto dos botões em hexadecimal.
 * @param colorBack 	Cor de fundo bos botões em hexadecimal.
 */
function desableEnableMenuGraph(opacity, background, disabled, colorText, colorBack){
	// Gráfico
	$("#chart_div").css({ "opacity": opacity });
	$("#filter_div").css({ "opacity": opacity });
	$("#filter_mobile_div").css({ "opacity": opacity });
	$("#dashboard_div").css({ "background": background });
	// Botão Período.
	$(".grafico-periodo-label").prop("disabled", disabled);
	$(".grafico-periodo-label").css({ "color": colorText, "background-color": colorBack });	
	// Botão Enviar.
	$(".enviar-data-grafico").prop("disabled", disabled);
	$(".enviar-data-grafico").css({ "color": colorText, "background-color": colorBack });	
	// Botão Dia.
	$("#dia").prop("disabled", disabled);
	$("#dia").css({ "color": colorText, "background-color": colorBack });
}

/**
 * Reproduz a animação de fechamento do gráfico.
 */
function fechardados(){
	$("#dadosLocal").css({ "left" : "-100%" });
	$("#dados").animate({ "height" : dadosHeight }, 500);
	setTimeout(function(){ $("#dados").css({ "height" : "auto" })}, 1000);
}

/**
 * Carrega dados a partir das opções selecionadas pelo usuário.
 * @param local Local selecionado.
 */
function carregardados(local){
	
	// Reproduz a animação de acordo com o tamanho da tela.
	if($(window).width() < 768){
		$('html, body').animate({
			scrollTop: ($("#dados").offset().top - 50)
		}, 500);
		$("#dados").animate({ "height" : window.innerHeight + 150 }, 500, function(){ $("#dadosLocal").css({ "left" : "0%" }); });
	} else {
		$('html, body').animate({
			scrollTop: ($("#dados").offset().top - 0)
		}, 500);
		$("#dados").animate({ "height" : window.innerHeight }, 500, function(){ $("#dadosLocal").css({ "left" : "0%" }); });
	}	
	
	// Verifica o nome do local selecionado.
	if(local == "Largo da Batata"){
		painel = "Barulhometro001";
		local = "Dados do Largo da Batata.";
	} else if(local == "Parque Ibirapuera"){
		painel = "Barulhometro002";
		local = "Dados do Parque Ibirapuera.";
	}
	
	// Atribui a legenda do local.
	$('#titulodados').html(local);	
	
	coletaDadosGrafico();
	
	// Captura a altura do elemento.
	dadosHeight = $("#dados").height() + 30;
}