var express = require('express');
var router = express.Router();
var path = require('path');
const WebSocket = require('ws');
var mongoose = require('mongoose');

const wss = new WebSocket.Server({port: 8002});

wss.on('connection', function connection(ws) {
  ws.on('message', function incoming(message) {
    console.log('received: %s', message);
  });

});

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/sth_stoic', {useNewUrlParser: true});
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'Erro de conexão ao MongoDB:'));
db.once('open', function(){
    console.log("Conectado ao MongoDB");
});

Barulhometro = require('../models/barulhometroModel');
Barulhometro2 = require('../models/barulhometro2Model');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.sendFile(path.join(global.__basedir + '/views/barulhometro.html'));
});

router.post('/barulhometro/spl', function(req, res) { 
  //var value = req.body.contextResponses[0].contextElement.attributes[0].value;
  var value = req.body.data[0].noise.value;
  var parks = {
    "Barulhometro001": 1,
    "Barulhometro002": 2
  }
  var sendMessage = parks[req.body.data[0].id] + "\t\t" + value; //TODO: Colocar o valor correto do painel
  wss.clients.forEach(function (client){
      if (client.readyState === WebSocket.OPEN){
          client.send(sendMessage);
      }
  });
  //console.log(sendMessage);
  res.status(204).send();
});

module.exports = router;
