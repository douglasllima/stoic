var express = require('express');
var router = express.Router();
var barulhometro = require('../controllers/barulhometroController');

/* GET users listing. */
router.get('/', barulhometro.listSPL);

module.exports = router;