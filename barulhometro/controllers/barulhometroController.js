'use strict';

var mongoose = require('mongoose');
const request = require('request');
var path = require('path');
var soundUtils = require('../utils/SoundUtils');

Barulhometro = mongoose.model('barulhometro');
// Barulhometro2 = mongoose.model('barulhometro2')
var Schema = mongoose.Schema;

var barulhometroDeviceSchema = new Schema({
    recvTime :  Date,
    attrName :  String,
    attrType :  String,
    attrValue : String
});



exports.listSPL = function(req, res){

    var local = "";
    var idPainel = req.query.painel;
     //console.log("IdPainel " + idPainel);
    var callback = req.query.barulhometroCallback;
     //console.log(callback);
    var strDataEscolhida = req.query.data.split('/');
    var dataEscolhida = new Date(strDataEscolhida[2], strDataEscolhida[1]-1,strDataEscolhida[0], 0, 0, 0, 0);    
     console.log(dataEscolhida);

    var horaInicio = new Date(dataEscolhida);
    var horaFim = new Date(dataEscolhida);

    switch(req.query.periodo){
        case("Madrugada"):
            horaFim.setHours(6);
            break;
        case("Manha"):
            horaInicio.setHours(6);
            horaFim.setHours(12);
            break;
        case("Tarde"):
            horaInicio.setHours(12);
            horaFim.setHours(18);
            break;
        case("Noite"):			
            horaInicio.setHours(18);
            horaFim.setHours(24);       
            break;            
    }
     console.log("Inicio: " + horaInicio);
     console.log("Fim: " + horaFim);

    let headers = {
        "fiware-service": "stoic",
        "fiware-servicepath": "/"
    }

    request('http://localhost:1026/v2/entities/' + idPainel + '?options=keyValues', {json:true, headers:headers}, (err, resA, body) => {       //TODO: Especificar o dispositivo dinamicamente            

        if (err){return console.error(err);}

        if (!resA.body.error) {
            //console.log(resA.body)
            local = resA.body.address.description;   
            
            console.log(JSON.stringify({recvTime : {'$gte': horaInicio , '$lt': horaFim}}));

            let model = mongoose.model(idPainel, barulhometroDeviceSchema,`sth_/_${idPainel}_Barulhometro`);

            Barulhometro.find({attrName: "noise", recvTime : {'$gte': horaInicio , '$lt': horaFim}}, null, {sort: 'recvTime'}, function(err, data){ 

                var result = [];

                if (err)
                {
                    res.send(err);
                    console.error(err);
                } else if(data.length > 0) {                                                        

                    var i = 0;
                    var j = 0;
                    var splByMinute = [];
                    var di = data[0].recvTime;
                    var inicialTime = [new Date(di.getFullYear(), di.getMonth(), di.getDate(), di.getHours(), di.getMinutes(), 0, 0)];

                    data.forEach(d => {

                        if ((d.recvTime.getHours()*60 + d.recvTime.getMinutes()) > (inicialTime[i].getHours()*60 + inicialTime[i].getMinutes())){
                            j = 0;
                            i++;
                        }
                        else {
                            j++;
                        }
                        
                        if (!splByMinute[i]){
                            splByMinute[i] = [];
                        }
                        
                        splByMinute[i].push(d.attrValue);

                        if (j == 0){
                            var dt = d.recvTime;
                            inicialTime[i] = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate(), dt.getHours(), dt.getMinutes(), 0, 0);
                        }

                    });

                    //console.log(inicialTime);

                    for (var it = 0; it < splByMinute.length; it++){

                            result.push(
                            {
                                "nivelSonoro" : soundUtils.calculaLeq(splByMinute[it]),
                                "data" : inicialTime[it],
                                "painel" :{
                                    "id":idPainel,
                                    "local":local
                                }
                            }
                        );
                    }
                }
                // console.log(result);
                if (callback){
                    res.send(callback+ '(' + JSON.stringify(result) + ')');
                } else {
                    res.send(result);
                }
            });        
        } else {
            res.sendStatus(404);
        }
    });  
}
