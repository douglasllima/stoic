from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
from .forms import LoginForm
import json

def index(request):
    if request.user.is_authenticated:
        print (request.user.id)
        return render(request, 'index.html')
        #return render(request, 'index.html', {'active': get_active(request) })
    else:
        if request.method == 'POST':
            form = LoginForm(request.POST)
            if form.is_valid():
                username = form.cleaned_data.get('username')
                raw_password = form.cleaned_data.get('password')
                user = authenticate(username=username, password=raw_password)
                login(request, user)
                return redirect('/')
    form = LoginForm()
    return render(request, 'index.html', {'form': form})

def get_active(request):
    workspace = './workspace/%s' % request.user.id
    with open(workspace + '/.config', 'r') as config:
        cfg = json.loads(config.read().strip())
        if "active" in cfg:
            active = cfg["active"]
        else:
            active = None
    return active
