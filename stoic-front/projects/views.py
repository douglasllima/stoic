from django.shortcuts import render
import os
from django.contrib.auth.decorators import login_required
from datetime import datetime
from .forms import ProjectForm, ProjectEditForm
from django.http import *
import uuid
import shutil
import json
from .middlewares import *

@login_required
def index(request):
    workspace = './workspace/%s' % request.user.id
    files = []
    if not os.path.exists(workspace):
        os.makedirs(workspace)
        os.mknod(workspace + '/.config')
        with open(workspace + '/.config', 'wb') as config:
            config.write("{}".encode())
    active = get_active(request)
    # r=root, d=directories, f = files
    for r, d, f in os.walk(workspace):
        for directory in d:
            ts = int(os.path.getmtime(workspace + '/' + directory))
            files.append({
                'name': directory,
                'last_modified': datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S'),
                'active': True if active == directory else False
            })
    print(files)
    return render(request, 'projects/index.html', { 'files': files, 'form': ProjectForm(), 'active': active })

@login_required
def new(request):
    if request.method == 'POST':
        form = ProjectForm(request.POST, request.FILES)
        workspace = './workspace/%s' % request.user.id
        f = request.FILES['file']
        if form.is_valid():
            f = request.FILES['file']
            #with open(workspace + '/' + str(uuid.uuid1()) + '.json', 'wb+') as destination:
            if not os.path.exists(workspace + '/' + form.cleaned_data['name']):
                name = form.cleaned_data['name']
            else:
                i = 0
                while os.path.exists(workspace + '/' + form.cleaned_data['name'] + '(%s)' % i):
                    i += 1
                name = form.cleaned_data['name'] + "(%s)" % i
            os.makedirs(workspace + '/' + name)
            with open(workspace + '/' + name + '/project.json', 'wb+') as destination:
                for chunk in f.chunks():
                    destination.write(chunk)
            return HttpResponseRedirect('/projects')
        else:
            print("Not valid")

@login_required
def edit(request):
    workspace = './workspace/%s' % request.user.id
    if request.method == 'GET':
        data = {}
        data['name'] = request.GET["name"]
        with open(workspace + '/' + data['name'] + '/project.json', "rb") as file:
            data['text'] = file.read().decode()
        form = ProjectEditForm(initial = data)
        print(form)
        return render(request, 'projects/edit.html', { 'data': data, 'form': form, 'active': get_active(request) })
    elif request.method == 'POST':
        form = ProjectEditForm(request.POST)
        if form.is_valid():
            with open(workspace + '/' + form.cleaned_data['name'] + '/project.json', "wb") as file:
                file.write(form.cleaned_data.get('text').encode())
            return HttpResponseRedirect('/projects')
        else:
            print("Not valid")

@login_required
def delete(request):
    if request.method == 'GET':
        name = request.GET['name']
        workspace = './workspace/%s' % request.user.id
        config = workspace + '/.config'
        with open(config, 'r') as file:
            cfg = json.loads(file.read().strip())
            if "active" in cfg:
                active = cfg["active"]
            else:
                active = None
        if active == name:
            cfg.pop('active')
            with open(config, "w+") as file:
                file.truncate()
                json.dump(cfg, file)
        if os.path.exists(workspace + '/' + name):
            shutil.rmtree(workspace + '/' + name)
        else:
            print("The file does not exist")
        return HttpResponseRedirect('/projects')

@login_required
def activate(request):
    # global brokers
    workspace = './workspace/%s' % request.user.id
    if request.method == 'GET':
        proj = request.GET["name"]
        if os.path.exists(workspace + '/' + proj):
            workspace = './workspace/%s' % request.user.id
            config = workspace + '/.config'
            with open(config, "r") as file:
                cfg = json.load(file)
                cfg["active"] = proj
                '''
                try:
                    brokers
                except NameError:
                    pass
                else:
                    del brokers
                brokers = {}
                with open(workspace + '/%s/%s' % (proj, "project.json"), "r") as proj_file:
                    p = json.load(proj_file)
                    for broker, content in p['brokers'].items():
                        _type = content['type']
                        if _type == 'fiware':
                            mw = fiware_middleware
                        elif _type == 'watson':
                            mw = watson_middleware
                        brokers[broker] = mw(content, None)
                '''
            with open(config, "w+") as file:
                file.truncate()
                json.dump(cfg, file)
        return HttpResponseRedirect('/projects')

def get_active(request):
    workspace = './workspace/%s' % request.user.id
    with open(workspace + '/.config', 'r') as config:
        cfg = json.loads(config.read().strip())
        if "active" in cfg:
            active = cfg["active"]
        else:
            active = None
    return active