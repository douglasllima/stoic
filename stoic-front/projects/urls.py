from django.urls import path
from . import views

app_name = 'projects'
urlpatterns = [
    path('', views.index, name='index'),
    path('new', views.new, name='new'),
    path('edit', views.edit, name='edit'),
    path('delete', views.delete, name='delete'),
    path('activate', views.activate, name='activate')
]
