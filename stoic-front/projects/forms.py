from django.contrib.auth.forms import UserCreationForm
from django import forms

class LoginForm(forms.Form):
    username = forms.CharField(label='Username', max_length=30)
    password = forms.CharField(label='Password', max_length=30, widget=forms.PasswordInput)

class SingUpForm(forms.Form):
    username = forms.CharField(label='Username', max_length=30)
    password = forms.CharField(label='Password', max_length=30, widget=forms.PasswordInput)

class ProjectForm(forms.Form):
    name = forms.CharField(label='Project name', max_length=30)
    file = forms.FileField(label='Project file')


class ProjectEditForm(forms.Form):
    name = forms.CharField(widget=forms.HiddenInput())
    text = forms.CharField(label='', widget=forms.Textarea(attrs={'rows':25}))