from colorama import Fore, Back, Style
import pystache
import requests
import json

def log(label, message, level = 'debug', enable = True):
    if level == 'debug':
        level_color = (Back.BLUE, Fore.BLUE)
    elif level == 'verbose':
        level_color = (Back.WHITE, Fore.WHITE)
    elif level == 'warning':
        level_color = (Back.YELLOW, Fore.YELLOW)
    elif level == 'error':
        level_color = (Back.RED, Fore.RED)
    elif level == 'info':
        level_color = (Back.LIGHTMAGENTA_EX, Fore.LIGHTMAGENTA_EX)
    elif level == 'success':
        level_color = (Back.GREEN, Fore.GREEN)

    if enable:
        print("{0}[{1}]{2} => {3}{4}{5}".format(level_color[0], label, Style.RESET_ALL, level_color[1], message, Style.RESET_ALL))

TAG = "DOUGLAS"

class BaseDict(dict):
    def from_dict(self, source):
        for k, v in source.items():
            setattr(self, k, v)
    
    def from_json(self, source):
        self.from_dict(json.loads(source))
    
    def to_dict(self):
        return self.__dict__
    
    def to_json(self):
        return json.dumps(self.to_dict())

    def __getitem__(self, key):
        return getattr(self, key)
    
    def __setitem__(self, key, value):
        setattr(self, key, value)
    
    def __str__(self):
        return str(self.to_dict())

class Device(BaseDict):
    def __init__(self, _type, _id, description = None):
        self.type = _type
        self.id = _id
        self.description = description
    
    @staticmethod
    def build(device):
        d = Device(_type = device['type'], _id = device['id'])
        if 'description' in device:
            d['description'] = device['description']
        return d

class Entity(BaseDict):
    def __init__(self, _type, _id):
        self['type'] = _type
        self['id'] = _id
        self.attrs = {}
    
    def add_attr(self, name, value, _type, metadata = None):
        attr = {
            'value': value,
            'type': _type,
        }
        if metadata:
            attr['metadata'] = metadata
        self.attrs[name] = attr

    def remove_attr(self, name):
        del attrs[name]
    
    def get_type(self):
        return self.type
    
    def get_id(self):
        return self.id
    
    def get_broker(self):
        return self.broker
    
    def set_type(self, _type):
        self.type = _type
    
    def set_id(self, _id):
        self.id = _id
    
    def set_broker(self, broker):
        self.broker = broker

    @staticmethod
    def build(entity):
        ent = Entity(entity['type'], entity['id'])
        del entity['type']
        del entity['id']
        for name, obj in entity.items():
            if 'metadata' in obj:
                ent.add_attr(name, obj['value'], obj['type'], metadata=obj['metadata'])
            else:
                ent.add_attr(name, obj['value'], obj['type'])
        return ent

    def __getitem__(self, item):
        if item == 'id':
            return self.get_id()
        elif item == 'type':
            return self.get_type()
        elif item == 'broker':
            return self.get_broker()
        elif item in self.attrs:
            return self.attrs[item]['value']
        else:
            raise KeyError(item)
    
    def __setitem__(self, item, value):
        if item == 'id':
            return self.set_id(value)
        elif item == 'type':
            return self.set_type(value)
        elif item == 'broker':
            return self.set_broker(value)
        elif item in self.attrs:
            self.attrs[item] = value
        else:
            raise KeyError(item)
    
    def to_dict(self):
        d = dict(
            id=self.id,
            type=self.type
        )
        d.update(self.attrs)
        return d

class Association(BaseDict):
    def __init__(self, _id, device_type, device_id, entity_type, entity_id, mapping, description = None):
        self.id = _id
        self.device_type = device_type
        self.device_id = device_id
        self.entity_type = entity_type
        self.entity_id = entity_id
        self.mapping = mapping if mapping is not None else {}
        self.description = description
    
    def bind_entity(self, entity):
        self.entity_type = entity['type']
        self.entity_id = entity['id']
    
    def bind_device(self, device):
        self.device_type = device['type']
        self.device_id = device['id']

    def map(self, device_attr, entity_attr, entity_attr_type):
        self.mapping.update({
            device_attr: { 
                'name': entity_attr,
                'type': entity_attr_type
            }
        })

    @staticmethod
    def build(association):
        a = Association(
            association['id'] if 'id' in association else None,
            association['device_type'],
            association['device_id'],
            association['entity_type'],
            association['entity_id'],
            association['mapping'],
            description = association['description'] if 'description' in association else None
        )
        return a

class Action(BaseDict):
    def __init__(self, _id, method, target, data, headers={}, params={}, description=None):
        self.id = _id
        self.method = method
        self.target = target
        self.headers = headers
        self.params = params
        self.data = data
        self.description = description
    
    def invoke(self, vars={}):
        action = self.__dict__
        log(TAG, vars)
        log(TAG, pystache.render(action['data'], vars))
        try:
            r = requests.request(method=action['method'], url=action['target'], headers=action['headers'], params=action['params'], data=pystache.render(action['data'], vars))
            return r.text
        except requests.exceptions.RequestException as e:
            log(TAG, "Error connection")
            log(TAG, e)

    @staticmethod
    def build(action):
        return Action(
            _id = action['id'] if 'id' in action else None,
            method = action['method'],
            target = action['target'],
            headers = action['headers'],
            params = action['params'],
            data = action['data'],
            description = action['description'] if 'description' in action else None
        )

class Trigger(BaseDict):
    def __init__(self, _id, entity_type, entity_id, condition, description=None):
        self.id = _id
        self.entity = None
        self.entity_type = entity_type
        self.entity_id = entity_id
        self.condition = condition
        self.description = description
    
    def bind_entity(self,entity):
        self.entity_type = entity['type']
        self.entity_id = entity['id']
        self.entity = entity
    
    def evaluate(self):
        eval("print(light)",self.entity)
        return eval(self.condition, self.entity)
    
    @staticmethod
    def build(trigger):
        return Trigger(
            _id = trigger['id'] if 'id' in trigger else None,
            entity_type = trigger['entity_type'],
            entity_id = trigger['entity_id'],
            condition = trigger['condition'],
            description = trigger['description']
        )

class Task(BaseDict):
    def __init__(self, _id, trigger_id, action_id, entity_type = None, entity_id = None):
        self.id = _id
        self.trigger_id = trigger_id
        self.action_id = action_id
        self.entity_type = entity_type
        self.entity_id = entity_id
    
    def bind_trigger(self, trigger):
        self.trigger = trigger.id

    def bind_action(self, action):
        self.action = action.id

    def set_entity(self, entity_type, entity_id):
        self.entity_type = entity_type
        self.entity_id = entity_id
    
    @staticmethod
    def build(task):
        return Task(
            _id = task['id'] if 'id' in task else None,
            trigger_id = task['trigger_id'],
            action_id = task['action_id']
        )