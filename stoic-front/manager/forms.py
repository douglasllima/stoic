from django import forms

class DeviceForm (forms.Form):
    broker = forms.ChoiceField(label='Broker')
    _type = forms.CharField(label='Type', max_length=30)
    _id = forms.CharField(label='ID', max_length=30)
    description = forms.CharField(label='Description', widget=forms.Textarea(attrs={'rows':5}), max_length=100)

class EntityForm (forms.Form):
    brokers = forms.MultipleChoiceField(label='Brokers', widget=forms.SelectMultiple(attrs = {'name':'brokers','class':'selectpicker'}))
    _type = forms.CharField(label='Type', max_length=30, widget=forms.TextInput(attrs={'id':'entity_type'}))
    _id = forms.CharField(label='ID', max_length=30, widget=forms.TextInput(attrs={'id':'entity_id'}))

class AttributeForm (forms.Form):
    _name = forms.CharField(label='Type', max_length=30)
    _value = forms.CharField(label='Type', max_length=30)
    _type = forms.CharField(label='Type', max_length=30)
    _metadata = forms.CharField(label='Type', max_length=30)

# class AssociationForm (forms.Form):
#     broker = forms.CharField(label='Broker', max_length=30)
#     device_type = forms.CharField(label='Device Type', max_length=30)
#     device_id = forms.CharField(label='Device ID', max_length=30)
#     entity_type = forms.CharField(label='Entity Type', max_length=30)
#     entity_id = forms.CharField(label='Entity ID', max_length=30)

class AssociationForm (forms.Form):
    broker = forms.ChoiceField(label='Broker ID', widget=forms.Select(attrs={'id':'broker_id'}))
    device_type = forms.ChoiceField(label='Device Type', widget=forms.Select(attrs={'id':'device_type'}))
    device_id = forms.ChoiceField(label='Device ID', widget=forms.Select(attrs={'id':'device_id'}))
    entity_type = forms.ChoiceField(label='Entity Type', widget=forms.Select(attrs={'id':'entity_type'}))
    entity_id = forms.ChoiceField(label='Entity ID', widget=forms.Select(attrs={'id':'entity_id'}))

class MappingForm (forms.Form):
    device_attr = forms.CharField(label='Type', max_length=30)
    entity_attr = forms.CharField(label='Type', max_length=30)
    entity_attr_type = forms.CharField(label='Type', max_length=30)

class TaskForm(forms.Form):
    broker = forms.ChoiceField(label='Broker ID', widget=forms.Select(attrs={'id':'broker_id'}))
    #text = forms.CharField(label='', widget=forms.Textarea(attrs={'rows':25}))

class TaskEditForm(forms.Form):
    broker = forms.ChoiceField(label='Broker ID', widget=forms.Select(attrs={'id':'edit_broker_id'}))