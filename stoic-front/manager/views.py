from django.shortcuts import render
from django.http import *

from .middlewares import *
from .elements import *
from .forms import DeviceForm, EntityForm, AssociationForm, TaskForm, TaskEditForm
import re

from django.views.decorators.csrf import csrf_exempt

from django.contrib.auth.decorators import login_required

from django.urls import reverse

from requests import request
'''
wiotp_broker = {
    "id": "broker1",
    "type": "watson",
    "url": "http://gvng6v....",
    "description": "desc",
    "auth": {
        "key": "a-gv6isn-xehobeg6qr",
        "token": "Lzn&I7Id6oqqKwjDkA"
    },
    "mqtt": {
        "port": 1883,
        "transport": "tcp|websockets",
        "cleanStart": True|False,
        "sessionExpiry": 3600,
        "keepAlive": 60
    },
    "watson": {
        "org": "gv6isn"
    },
    "cloudant": {
        'user': "6c505913-4696-4168-b63c-4354e4b13730-bluemix",
        'password': "ec07709b77140b43000189bf4c2ba78db446798860c55bfa80f19b7c2cc84aa3",
        'url': "https://6c505913-4696-4168-b63c-4354e4b13730-bluemix:ec07709b77140b43000189bf4c2ba78db446798860c55bfa80f19b7c2cc84aa3@6c505913-4696-4168-b63c-4354e4b13730-bluemix.cloudantnosqldb.appdomain.cloud",
        'db_entities': 'stoic_entities',
        'db_associations': 'stoic_associations',
        'db_actions': 'stoic_actions',
        'db_triggers': 'stoic_triggers',
        'db_tasks': 'stoic_tasks'
    }
}

f_broker = {
    "iota": {
        'url': 'http://localhost:4061/iot',
        'headers': {
            'fiware-service': 'stoic',
            'fiware-servicepath': '/',
            'cache-control': "no-cache"
        }
    },
    "orion": {
        "url": 'http://localhost:1026'
    },
    "cygnus": {
        "url": 'http://127.0.0.1:5050',
        'notify_root': '/notify'
    },
    "mongodb": {
        "url": "mongodb://localhost:27017/"
    },
    "historical": {
        "activate": True
    }
}

f_obj = fiware_middleware(f_broker, None)
'''
@login_required
def brokers_index(request):
    #broker_form = BrokerForm()
    r = requests.get("http://localhost:8080/broker")
    brokers = r.json()

    broker_form = BrokerForm()
    broker_form.fields['_type'].choices = [(0, 'Watson'), (1, 'Fiware')]
    return render(request, 'brokers/index.html', { 'brokers': brokers, 'form': broker_form })
    

@login_required
def devices_index(request):
    #devices = f_obj.list_devices()
    form = DeviceForm()
    #print(update_brokers(request))
    # try:
    #     brokers
    # except NameError:
    #     update_brokers(request)
    # devices = []
    # for name, broker in brokers.items():
    #     d = broker.list_devices()
    #     for el in d:
    #         el['broker'] = name
    #     devices += d
    # if 'Content-Type' in request.headers:
    #     if request.headers['Content-Type'] == 'application/json':
    #         return devices
    r = requests.get("http://localhost:8080/broker")
    brokers = r.json()
    choices = []
    for b in brokers:
        choices.append((b['id'], b['id'] + " [" + b['type'] + "]"))
    form.fields['broker'].choices = choices
    r = requests.get('http://localhost:8080/device')
    devices = r.json()
    new_devices = []
    for broker, devices_list in devices.items():
        for d in devices_list:
            d['broker'] = broker
            new_devices.append(d)
    if request.headers.get('Accept') == 'application/json':
        return JsonResponse(data = new_devices, safe = False)  
    return render(request, 'devices/index.html', { 'devices': new_devices, 'form': form })
    #return render(request, 'devices/index.html', { 'devices': new_devices, 'form': form })

@login_required
def devices_new(request):
    if request.method == 'GET':
        form = DeviceForm()
        return render(request, 'devices/new.html', { 'form': form, 'action': 'new' })
    elif request.method == 'POST':
        form = DeviceForm(request.POST)

        #if form.is_valid():
        _type = request.POST.get('_type')
        _id = request.POST.get('_id')
        description = request.POST.get('description')
        _broker = request.POST.get('broker')
        # try:
        #     brokers
        # except NameError:
        #     update_brokers(request)
        #brokers[_broker].create_device(Device(_type, _id))
        headers = {
            "broker": _broker,
            "content-type": "application/json"
        }
        r = requests.post("http://localhost:8080/device", data=json.dumps(Device(_type, _id, description).to_dict()), headers = headers)
        print(r.json())
        #f_obj.create_device(Device(_type,_id, description))
        return HttpResponseRedirect('/manager/devices')

@login_required
def devices_delete(request):
    if request.method == 'GET':
        _type = request.GET["type"]
        _id = request.GET["id"]
        _broker = request.GET['broker']
        # try:
        #     brokers
        # except NameError:
        #     update_brokers(request)
        r = requests.delete("http://localhost:8080/device/" + _type + "_" + _id, headers={'broker':_broker})
        if r.json():
            return HttpResponseRedirect('/manager/devices')
        else:
            return HttpResponseBadRequest(content='Error when trying to delete the device (%s:%s)' % (_type, _id))
    return HttpResponseBadRequest(content='Method %s not allowed. Use GET' % request.method)

@login_required
def entities_index(request):
    #devices = f_obj.list_devices()
    entity_form = EntityForm()

    r = requests.get("http://localhost:8080/broker")
    brokers = r.json()
    choices = []
    for b in brokers:
        choices.append((b['id'], b['id'] + " [" + b['type'] + "]"))
    #entity_form.fields['broker'].choices = choices
    entity_form.fields['brokers'].choices = choices
    if request.headers.get('Accept') != 'application/json':
        r = requests.get("http://localhost:8080/entity", params=dict(clustered = True))
        entities = r.json()
    else:
        if request.GET.get('clustered') == 'true':
            r = requests.get("http://localhost:8080/entity", params=dict(clustered = True))
            entities = r.json()
        else:
            r = requests.get("http://localhost:8080/entity")
            old_entities = r.json()
            entities = []
            for broker, ents in old_entities.items():
                for entity in ents:
                    entity['broker'] = broker
                    entities.append(entity)
    if r.status_code >= 300:
        return JsonResponse(data = dict(status = "error", message = r.text))
    if request.headers.get('Accept') == 'application/json':
        return JsonResponse(data = entities, safe = False)
    return render(request, 'entities/index.html', { 'entities': entities, 'form': entity_form })

@csrf_exempt
@login_required
def entities_new(request):
    if request.method == 'GET':
        form = EntityForm()
        return render(request, 'entities/new.html', { 'form': form, 'action': 'new' })
    elif request.method == 'POST':
        entity = json.loads(request.body)
        attrs = entity.pop("attrs")
        brokers = entity.pop("brokers")
        for attr in attrs:
            if 'metadata' in attr:
                if not isinstance(attr['metadata'], dict):
                    attr['metadata'] = json.loads(attr['metadata'])
            entity[attr.pop("name")] = attr
        #for broker in brokers:
        headers = {
            'content-type': 'application/json',
            'brokers': ','.join(brokers)
        }
        r = requests.post("http://localhost:8080/entity",data=json.dumps(entity),headers=headers)
        print(r.json())
        return HttpResponseRedirect('/manager/entities')

@csrf_exempt
@login_required
def entities_edit(request):
    entity = json.loads(request.body)
    attrs = entity.pop("attrs")
    brokers = entity.pop("brokers")

    # attrs.append(dict(
    #     name="tangle",
    #     type="boolean",
    #     value=True
    # ))
    for attr in attrs:
        entity[attr.pop("name")] = attr
    for broker in brokers:
        headers = {
            'content-type': 'application/json',
            'broker': broker
        }
        r = requests.put("http://localhost:8080/entity",data=json.dumps(entity),headers=headers)
        print(r.text)
    return HttpResponseRedirect(reverse('manager:entities_index'))

@login_required
def entities_delete(request):
    if request.method == 'GET':
        _type = request.GET.get("type")
        _id = request.GET.get("id")
        _brokers = request.GET.get('brokers')
        # try:
        #     brokers
        # except NameError:
        #     update_brokers(request)

        r = requests.delete("http://localhost:8080/entity/" + _id, headers=dict(brokers = _brokers), params=dict(type = _type))
        if r.status_code >= 200 and r.status_code < 300:
            return HttpResponseRedirect('/manager/entities')

        # for broker in _brokers:
        #     headers = { 'broker': broker }
        #     r = requests.delete("http://localhost:8080/entity/" + _id, headers=headers, params=dict(type = _type))
        #     if not r.json():
        #         return HttpResponseBadRequest(content='Error when trying to delete the entity (%s:%s)' % (_type, _id))
        #return HttpResponseRedirect('/manager/entities')
    return HttpResponseBadRequest(content='Method %s not allowed. Use GET' % request.method)

@login_required
def associations_index(request):
    association_form = AssociationForm()
    r = requests.get("http://localhost:8080/broker")
    brokers = r.json()
    choices = []
    for b in brokers:
        choices.append((b['id'], b['id'] + " [" + b['type'] + "]"))
    association_form.fields['broker'].choices = choices

    r = requests.get("http://localhost:8080/broker")
    brokers = r.json()
    associations = []
    for broker in brokers:
        headers = { 'broker':broker['id'] }
        r = requests.get("http://localhost:8080/association", headers=headers)
        for assoc in r.json():
            assoc['broker'] = broker['id']
            associations.append(assoc)
    
    return render(request, 'associations/index.html', { 'associations': associations, 'form': association_form })

@login_required
def associations_new(request):
    if request.method == 'GET':
        form = AssociationForm()
        return render(request, 'entities/new.html', { 'form': form, 'action': 'new' })
    elif request.method == 'POST':
        device_type = request.POST.get('device_type')
        device_id = request.POST.get('device_id')
        entity_type = request.POST.get('entity_type')
        entity_id = request.POST.get('entity_id')
        broker = request.POST.get('broker')
        association = Association(None, device_type, device_id, entity_type, entity_id, {})
        attrs = {}
        for key, value in request.POST.items():
            if key.startswith('attrs'):
                indice = re.search(r'(?<=\[)[0-9]+(?=\])', key).group(0)
                field = re.search(r'(?<=\[)[a-zA-Z_]+(?=\])', key).group(0)
                if not indice in attrs:
                    attrs[indice] = {}
                attrs[indice][field] = value
        for key, value in attrs.items():
            association.map(value['device_attr'], value['entity_attr'], value['entity_attr_type'])
        headers = {
            'content-type': 'application/json',
            'broker': broker
        }
        r = requests.post("http://localhost:8080/association",data=json.dumps(association.to_dict()),headers=headers)
        print(r.json())
        # try:
        #     brokers
        # except NameError:
        #     update_brokers(request)
        # brokers[_broker].create_entity(entity)

        #f_obj.create_device(Device(_type,_id, description))
        return HttpResponseRedirect('/manager/associations')

@login_required
def associations_delete(request):
    if request.method == 'GET':
        _id = request.GET["id"]
        _broker = request.GET['broker']
        # try:
        #     brokers
        # except NameError:
        #     update_brokers(request)
        headers = { 'broker': _broker }
        r = requests.delete("http://localhost:8080/association/" + _id, headers=headers)
        return HttpResponseRedirect('/manager/associations')
        # if r.json():
        #     return HttpResponseRedirect('/manager/associations')
        # else:
        #     return HttpResponseBadRequest(content='Error when trying to delete the association (%s)' % (_id))
    return HttpResponseBadRequest(content='Method %s not allowed. Use GET' % request.method)

@csrf_exempt
@login_required
def tasks_index(request):
    form = TaskForm()
    edit_form = TaskEditForm()
    r = requests.get("http://localhost:8080/broker")
    brokers = r.json()
    choices = []
    for b in brokers:
        choices.append((b['id'], b['id'] + " [" + b['type'] + "]"))
    form.fields['broker'].choices = choices
    edit_form.fields['broker'].choices = choices
    tasks = []
    r = requests.get("http://localhost:8080/task")
    tasks = r.json()
    print(tasks)
    _tasks = []
    for broker, tks in tasks.items():
        for tk in tks:
            tk["broker"] = broker
            _tasks.append(tk)
    
    if request.headers.get('Accept') != 'application/json':
        #form = JSONSchemaForm(schema=schema, options={"theme": "bootstrap4"}, ajax=True)
        return render(request, 'tasks/index.html', { 'tasks': _tasks, 'form': form, 'edit_form': edit_form })
    else:
        return JsonResponse(data=_tasks,safe=False)

@login_required
def tasks_new(request):
    if request.method == 'POST':
        task = json.loads(request.POST['task'])
        broker = request.POST.get('broker')
        r = requests.post("http://localhost:8080/task", data=json.dumps(task), headers={'content-type':'application/json', 'broker': broker})
        print(r.json())
        return HttpResponseRedirect(reverse('manager:tasks_index'))

@csrf_exempt
@login_required
def tasks_edit(request):
    if request.method == 'POST':
        task = json.loads(request.POST['task'])
        broker = task.pop('broker')
        r = requests.put("http://localhost:8080/task/" + task['id'], data=json.dumps(task), headers={'content-type':'application/json', 'broker': broker})
        return HttpResponseRedirect(reverse('manager:tasks_index'))    

@login_required
def tasks_delete(request):
    if request.method == 'GET':
        _id = request.GET['id']
        broker = request.GET['broker']
        r = requests.delete('http://localhost:8080/task/' + _id, headers={"broker": broker})
        print(r.text)
        return HttpResponseRedirect(reverse('manager:tasks_index'))

@login_required
def dashboard_index(request):
    if request.method == 'GET':
        return render(request, 'dashboard/index.html', {'delay': 1000} )

def dashboard_update(request):
    # try:
    #     brokers
    # except NameError:
    #     update_brokers(request)
    # entities = []
    # for name, broker in brokers.items():
    #     e = broker.list_entities()
    #     for el in e:
    #         el_dict = el.to_dict()
    #         el_dict['broker'] = name
    #         entities.append(el_dict)
    r = requests.get("http://localhost:8080/entity")
    old_entities = r.json()
    entities = []
    for broker, ents in old_entities.items():
        for entity in ents:
            entity['broker'] = broker
            entities.append(entity)
    return JsonResponse(entities, safe=False)

def get_active(request):
    workspace = './workspace/%s' % request.user.id
    with open(workspace + '/.config', 'r') as config:
        cfg = json.loads(config.read().strip())
        if "active" in cfg:
            active = cfg["active"]
        else:
            active = None
    return active

def get_active_text(request):
    active = get_active(request)
    if active:
        workspace = './workspace/%s' % request.user.id
        with open(workspace + '/' + active + '/project.json', "rb") as file:
            return file.read().decode()
    return None
 
# def update_brokers(request):
#     global brokers
#     brokers = {}
#     p = json.loads(get_active_text(request))
#     for broker, content in p['brokers'].items():
#         _type = content['type']
#         if _type == 'fiware':
#             mw = fiware_middleware
#         elif _type == 'watson':
#             mw = watson_middleware
#         brokers[broker] = mw(content, None)
#     return brokers