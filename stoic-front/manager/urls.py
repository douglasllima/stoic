from django.urls import path, re_path
from . import views

app_name = 'manager'

urlpatterns = [
    # -- BROKERS --
    #path('brokers', views.brokers_index, name='brokers_index'),
    #path('brokers/new', views.brokers_new, name='brokers_new'),
    #path('brokers/delete', views.brokers_delete, name='brokers_delete'),
    # -- DEVICES --
    path('devices', views.devices_index, name='devices_index'),
    path('devices/new', views.devices_new, name='devices_new'),
    path('devices/delete', views.devices_delete, name='devices_delete'),
    # -- ENTIDADES --
    path('entities', views.entities_index, name='entities_index'),
    path('entities/new', views.entities_new, name='entities_new'),
    path('entities/edit', views.entities_edit, name='entities_edit'),
    path('entities/delete', views.entities_delete, name='entities_delete'),
    # -- ASSOCIATIONS --
    path('associations', views.associations_index, name='associations_index'),
    path('associations/new', views.associations_new, name='associations_new'),
    path('associations/delete', views.associations_delete, name='associations_delete'),
    # -- TASKS --
    path('tasks', views.tasks_index, name='tasks_index'),
    path('tasks/new', views.tasks_new, name='tasks_new'),
    path('tasks/edit', views.tasks_edit, name='tasks_edit'),
    path('tasks/delete', views.tasks_delete, name='tasks_delete'),
    # -- DASHBOARD --
    path('dashboard', views.dashboard_index, name='dashboard_index'),
    path('dashboard/update', views.dashboard_update)
]
'''
Autorização para utilizar a reserva do auxilio a pesquisa
Encaminhar a solicitação pelo sistema Agilis fapesp.br/rt 21 de dezembro
'''
