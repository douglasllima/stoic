from django.shortcuts import render
from django.http import *

from .middlewares import *
from .elements import *
from .forms import DeviceForm, EntityForm
import re

from django.contrib.auth.decorators import login_required
'''
wiotp_broker = {
    "id": "broker1",
    "type": "watson",
    "url": "http://gvng6v....",
    "description": "desc",
    "auth": {
        "key": "a-gv6isn-xehobeg6qr",
        "token": "Lzn&I7Id6oqqKwjDkA"
    },
    "mqtt": {
        "port": 1883,
        "transport": "tcp|websockets",
        "cleanStart": True|False,
        "sessionExpiry": 3600,
        "keepAlive": 60
    },
    "watson": {
        "org": "gv6isn"
    },
    "cloudant": {
        'user': "6c505913-4696-4168-b63c-4354e4b13730-bluemix",
        'password': "ec07709b77140b43000189bf4c2ba78db446798860c55bfa80f19b7c2cc84aa3",
        'url': "https://6c505913-4696-4168-b63c-4354e4b13730-bluemix:ec07709b77140b43000189bf4c2ba78db446798860c55bfa80f19b7c2cc84aa3@6c505913-4696-4168-b63c-4354e4b13730-bluemix.cloudantnosqldb.appdomain.cloud",
        'db_entities': 'stoic_entities',
        'db_associations': 'stoic_associations',
        'db_actions': 'stoic_actions',
        'db_triggers': 'stoic_triggers',
        'db_tasks': 'stoic_tasks'
    }
}

f_broker = {
    "iota": {
        'url': 'http://localhost:4061/iot',
        'headers': {
            'fiware-service': 'stoic',
            'fiware-servicepath': '/',
            'cache-control': "no-cache"
        }
    },
    "orion": {
        "url": 'http://localhost:1026'
    },
    "cygnus": {
        "url": 'http://127.0.0.1:5050',
        'notify_root': '/notify'
    },
    "mongodb": {
        "url": "mongodb://localhost:27017/"
    },
    "historical": {
        "activate": True
    }
}

f_obj = fiware_middleware(f_broker, None)
'''
@login_required
def devices_index(request):
    #devices = f_obj.list_devices()
    form = DeviceForm()
    #print(update_brokers(request))
    try:
        brokers
    except NameError:
        update_brokers(request)
    devices = []
    for name, broker in brokers.items():
        d = broker.list_devices()
        for el in d:
            el['broker'] = name
        devices += d
    if 'Content-Type' in request.headers:
        if request.headers['Content-Type'] == 'application/json':
            return devices
    return render(request, 'devices/index.html', { 'devices': devices, 'form': form, 'active': get_active(request) })

@login_required
def devices_new(request):
    if request.method == 'GET':
        form = DeviceForm()
        return render(request, 'devices/new.html', { 'form': form, 'action': 'new', 'active': get_active(request) })
    elif request.method == 'POST':
        form = DeviceForm(request.POST)

        if form.is_valid():
            _type = form.cleaned_data['_type']
            _id = form.cleaned_data['_id']
            description = form.cleaned_data['description']
            _broker = form.cleaned_data['broker']
            try:
                brokers
            except NameError:
                update_brokers(request)
            brokers[_broker].create_device(Device(_type, _id))

            #f_obj.create_device(Device(_type,_id, description))
            return HttpResponseRedirect('/manager/devices')

@login_required
def devices_delete(request):
    if request.method == 'GET':
        _type = request.GET["type"]
        _id = request.GET["id"]
        _broker = request.GET['broker']
        try:
            brokers
        except NameError:
            update_brokers(request)
        if brokers[_broker].delete_device(Device(_type, _id)):
            return HttpResponseRedirect('/manager/devices')
        else:
            return HttpResponseBadRequest(content='Error when trying to delete the device (%s:%s)' % (_type, _id))
    return HttpResponseBadRequest(content='Method %s not allowed. Use GET' % request.method)

@login_required
def entities_index(request):
    #devices = f_obj.list_devices()
    entity_form = EntityForm()
    #print(update_brokers(request))
    try:
        brokers
    except NameError:
        update_brokers(request)
    entities = []
    for name, broker in brokers.items():
        e = broker.list_entities()
        for el in e:
            el['broker'] = name
        entities += e
    return render(request, 'entities/index.html', { 'entities': entities, 'form': entity_form, 'active': get_active(request) })

@login_required
def entities_new(request):
    if request.method == 'GET':
        form = DeviceForm()
        return render(request, 'entities/new.html', { 'form': form, 'action': 'new', 'active': get_active(request) })
    elif request.method == 'POST':
        _type = request.POST.get('_type')
        _id = request.POST.get('_id')
        _broker = request.POST.get('broker')
        entity = Entity(_type, _id)
        attrs = {}
        for key, value in request.POST.items():
            if key.startswith('attrs'):
                indice = re.search(r'(?<=\[)[0-9]+(?=\])', key).group(0)
                field = re.search(r'(?<=\[)[a-z]+(?=\])', key).group(0)
                if not indice in attrs:
                    attrs[indice] = {}
                attrs[indice][field] = value
        for key, value in attrs.items():
            entity.add_attr(value['name'], value['value'], value['type'])
        try:
            brokers
        except NameError:
            update_brokers(request)
        brokers[_broker].create_entity(entity)

        #f_obj.create_device(Device(_type,_id, description))
        return HttpResponseRedirect('/manager/entities')

@login_required
def entities_edit(request):
    if request.method == 'GET':
        form = DeviceForm()
        return render(request, 'entities/new.html', { 'form': form, 'action': 'new', 'active': get_active(request) })
    elif request.method == 'POST':
        _type = request.POST.get('_type')
        _id = request.POST.get('_id')
        _broker = request.POST.get('broker')
        entity = Entity(_type, _id)
        attrs = {}
        for key, value in request.POST.items():
            if key.startswith('attrs'):
                indice = re.search(r'(?<=\[)[0-9]+(?=\])', key).group(0)
                field = re.search(r'(?<=\[)[a-z]+(?=\])', key).group(0)
                if not indice in attrs:
                    attrs[indice] = {}
                attrs[indice][field] = value
        for key, value in attrs.items():
            entity.add_attr(value['name'], value['value'], value['type'])
        try:
            brokers
        except NameError:
            update_brokers(request)
        brokers[_broker].create_entity(entity)

        #f_obj.create_device(Device(_type,_id, description))
        return HttpResponseRedirect('/manager/entities')

@login_required
def entities_delete(request):
    if request.method == 'GET':
        _type = request.GET["type"]
        _id = request.GET["id"]
        _broker = request.GET['broker']
        try:
            brokers
        except NameError:
            update_brokers(request)
        if brokers[_broker].delete_entity(_type, _id):
            return HttpResponseRedirect('/manager/entities')
        else:
            return HttpResponseBadRequest(content='Error when trying to delete the entity (%s:%s)' % (_type, _id))
    return HttpResponseBadRequest(content='Method %s not allowed. Use GET' % request.method)

@login_required
def dashboard_index(request):
    if request.method == 'GET':
        return render(request, 'dashboard/index.html', {'active': get_active(request), 'delay': 1000} )

def dashboard_update(request):
    try:
        brokers
    except NameError:
        update_brokers(request)
    entities = []
    for name, broker in brokers.items():
        e = broker.list_entities()
        for el in e:
            el_dict = el.to_dict()
            el_dict['broker'] = name
            entities.append(el_dict)
    return JsonResponse(entities, safe=False)

def get_active(request):
    workspace = './workspace/%s' % request.user.id
    with open(workspace + '/.config', 'r') as config:
        cfg = json.loads(config.read().strip())
        if "active" in cfg:
            active = cfg["active"]
        else:
            active = None
    return active

def get_active_text(request):
    active = get_active(request)
    if active:
        workspace = './workspace/%s' % request.user.id
        with open(workspace + '/' + active + '/project.json', "rb") as file:
            return file.read().decode()
    return None
 
def update_brokers(request):
    global brokers
    brokers = {}
    p = json.loads(get_active_text(request))
    for broker, content in p['brokers'].items():
        _type = content['type']
        if _type == 'fiware':
            mw = fiware_middleware
        elif _type == 'watson':
            mw = watson_middleware
        brokers[broker] = mw(content, None)
    return brokers