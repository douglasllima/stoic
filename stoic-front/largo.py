import http.client
import ssl
import mimetypes
import time
from random import random

conn = http.client.HTTPSConnection("localhost", 7896, context = ssl._create_unverified_context())
payload = ''
headers = {
  'fiware-service': 'stoic',
  'fiware-servicepath': '/'
}
while True:
    conn.request("GET", "/iot/d?k=4jggokgpepnvsb2uv4s40d59ov&i=db_noise001&d=noise|" + str(random()*60), payload, headers)

res = conn.getresponse()
data = res.read()
print(data.decode("utf-8"))