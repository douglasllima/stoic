from django.shortcuts import render
import os
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime
from .forms import BrokerForm, BrokerEditForm
from django.http import *
import uuid
import shutil
import json
from .middlewares import *
import requests
from django_jsonforms.forms import JSONSchemaForm

@login_required
def index(request):
    brokers = []
    r = requests.get("http://localhost:8080/broker")
    brokers = r.json()
    print(brokers)
    
    if request.headers.get('Accept') != 'application/json':
        #form = JSONSchemaForm(schema=schema, options={"theme": "bootstrap4"}, ajax=True)
        new_form = BrokerForm()
        edit_form = BrokerEditForm()
        new_form.fields['_type'].choices = (('watson', 'Watson'), ('fiware', 'Fiware'))
        edit_form.fields['_type'].choices = (('watson', 'Watson'), ('fiware', 'Fiware'))
        return render(request, 'brokers/index.html', { 'brokers': brokers, 'new_form': new_form, 'edit_form': edit_form })
    else:
        return JsonResponse(data=brokers,safe=False)

@login_required
@csrf_exempt
def new(request):
    if request.method == 'POST':
        data = request.body.decode('utf-8')
        try:
            broker = json.loads(data)
        except:
            pass
        else:
            r = requests.post("http://localhost:8080/broker", data=json.dumps(broker), headers={'content-type':'application/json'})
        print(r.json())
        return HttpResponseRedirect('/brokers')

@login_required
@csrf_exempt
def edit(request):
    if request.method == 'GET':
        data = {}
        data['id'] = request.GET["id"]
        data['type'] = request.GET['type']
        r = requests.get("http://localhost:8080/broker/" + data['id'])
        attrs = r.json()
        attrs.pop('_id')
        attrs.pop('type')
        data['text'] = json.dumps(attrs)
        form = BrokerEditForm(initial = data)
        print(form)
        return render(request, 'brokers/edit.html', { 'data': data, 'form': form })
    elif request.method == 'POST':
        broker = json.loads(request.body.decode('utf-8'))
        r = requests.put("http://localhost:8080/broker", json.dumps(broker), headers={'content-type':'application/json'})
        return HttpResponseRedirect('/brokers')

@login_required
def delete(request):
    if request.method == 'GET':
        _id = request.GET['id']
        r = requests.delete('http://localhost:8080/broker/' + _id)
        r.json()
        return HttpResponseRedirect('/brokers')

@login_required
def schema(request, broker_type):
    if request.method == 'GET':
        r = requests.get('http://localhost:8080/broker/schema/' + broker_type)
        return JsonResponse(r.json(), safe=False)