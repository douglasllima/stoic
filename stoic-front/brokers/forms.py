from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.forms import ModelForm, Form
from django_jsonforms.forms import JSONSchemaField

class LoginForm(forms.Form):
    username = forms.CharField(label='Username', max_length=30)
    password = forms.CharField(label='Password', max_length=30, widget=forms.PasswordInput)

class SingUpForm(forms.Form):
    username = forms.CharField(label='Username', max_length=30)
    password = forms.CharField(label='Password', max_length=30, widget=forms.PasswordInput)

class BrokerForm(forms.Form):
    _type = forms.ChoiceField(label='Broker Type', widget= forms.Select(attrs={'id':'broker_type'}))
    _id = forms.CharField(label='Broker id', max_length=30, widget=forms.TextInput(attrs={'id':'broker_id'}))
    #text = forms.CharField(label='', widget=forms.Textarea(attrs={'rows':25}))

class BrokerEditForm(forms.Form):
    _type = forms.ChoiceField(label='Broker Type', widget=forms.Select(attrs={'id':'edit_broker_type'}))
    _id = forms.CharField(label='Broker Id', widget=forms.TextInput(attrs={'id':'edit_broker_id'}))
    #_type = forms.CharField(widget=forms.HiddenInput())
    #text = forms.CharField(label='', widget=forms.Textarea(attrs={'rows':25}))