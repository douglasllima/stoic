from pymongo import MongoClient
from bson.objectid import ObjectId

from swagger_server import utils
from swagger_server.middlewares import *

import json

def create_broker(broker):  # noqa: E501
    
    client = MongoClient('localhost', 27017)
    db_stoicws = client['stoic_ws']
    brokers = db_stoicws['brokers']

    broker['_id'] = broker['id']
    broker.pop('id')
    inserted_id = str(brokers.insert_one(broker).inserted_id)
    utils.get_brokers_connections(True)
    
    return inserted_id


def delete_broker(brokerId):  # noqa: E501

    client = MongoClient('localhost', 27017)
    db_stoicws = client['stoic_ws']
    brokers = db_stoicws['brokers']
    deleted_count = str(brokers.delete_one({'_id': brokerId}).deleted_count)
    utils.get_brokers_connections(True)
    
    return deleted_count

def get_broker_by_id(brokerId):  # noqa: E501
    client = MongoClient('localhost', 27017)
    db_stoicws = client['stoic_ws']
    brokers = db_stoicws['brokers']
    broker = brokers.find({'_id': brokerId})
    return list(broker)[0]

def list_brokers():  # noqa: E501
    client = MongoClient('localhost', 27017)
    db_stoicws = client['stoic_ws']
    brokers = db_stoicws['brokers']
    cursor = brokers.find({})
    lista_brokers = []
    for b in list(cursor):
        b['id'] = b['_id']
        b.pop('_id')
        lista_brokers.append(b)
    return lista_brokers

def update_broker(broker):  # noqa: E501
    client = MongoClient('localhost', 27017)
    db_stoicws = client['stoic_ws']
    brokers = db_stoicws['brokers']
    myquery = { "_id": broker["id"] }
    broker.pop("id")
    newvalues = { "$set": broker }
    upserted_id = brokers.update_one(myquery, newvalues).upserted_id
    utils.get_brokers_connections(True)

    return upserted_id

def get_schema_by_type(broker_type):
    if broker_type == 'fiware':
        return fiware_middleware.get_schema()
    elif broker_type == 'watson':
        return watson_middleware.get_schema()