from swagger_server.services import platforms_service
from swagger_server.middlewares import *

from swagger_server import utils

import requests as req

def list_entities(clustered = False):
    #brokers = utils.get_brokers_connections()
    # from id,broker in brokers.items():
    #     print(id, " - ", broker)
    brokers = utils.get_brokers_connections()
    print(brokers)
    entities = {}
    # for key, broker in brokers.items():
    #     try:
    #         entities[key]
    #     except:
    #         entities[key] = []
    #     entities[key] = list(map(lambda dev: dev.to_dict(), broker.list_entities()))
    # return entities
    for key, broker in brokers.items():
        try:
            entities[key]
        except:
            entities[key] = []
        entities[key] = list(map(lambda dev: dev.to_dict(), broker.list_entities()))

    if clustered:
        r = req.get("http://localhost:3000/clusters")
        clusters = r.json()
        clustered_entities = []
        for cluster in clusters:
            if len(cluster['brokers']) == 0:
                continue
            ents = entities[cluster['brokers'][0]]
            for broker in cluster['brokers']:
                ents = entities[broker]
                ent = {}
                for ent in ents:
                    if ent['id'] == cluster['entity_id'] and ent['type'] == cluster['entity_type']:
                        break
                if ent:
                    if broker == cluster['brokers'][0]:
                        clustered_entities.append(ent)
                    ent['brokers'] = cluster['brokers']
                    entities[broker].remove(ent)
        for broker in entities:
            ents = entities[broker]
            for ent in ents:
                ent['brokers'] = [broker]
                clustered_entities.append(ent)
        return clustered_entities
    else:
        return entities
    
    # clustered_entitites = {}
    # old_entities = entities.copy()
    # for key, ents in entities.items():
    #     for e in ents:
    #         #r = req.get("http://localhost:3000/clusters", params=dict(entity_id=e['id'], entity_type=e['type']))
    #         r = req.get("http://localhost:3000/clusters")
    #         clusters = r.json()
    #         for cluster in clusters:
    #             try:
    #                 clustered_entitites[e['type']]
    #             except:
    #                 clustered_entitites[e['type']] = {}
    #             try:
    #                 clustered_entitites[e['type']][e['id']]
    #             except:
    #                 clustered_entitites[e['type']][e['id']] = []
    #             has_broker = False
    #             for ent in clustered_entitites[e['type']][e['id']]:
    #                 if cluster[0] in ent['brokers']:
    #                     has_broker = True
    #             if not has_broker:
    #                 e['brokers'] = cluster
    #                 clustered_entitites[e['type']][e['id']].append(e)
    #                 old_entities[key].remove(e)

    # return clustered_entitites

def get_entity_by_id(brokerId, _type, _id):
    brokers = utils.get_brokers_connections()
    print(brokers)
    if brokerId in brokers:
        return brokers[brokerId].get_entity(_type, _id)
    else:
        return {'error': 'broker ' + brokerId + ' not found.'}, 400

def update_entity(brokerId, entity):
    brokers = utils.get_brokers_connections()
    print(brokers)
    if brokerId in brokers:
        _type = entity['type']
        _id = entity['id']
        entity.pop('type')
        entity.pop('id')
        return brokers[brokerId].update_entity(_type, _id, entity)
    else:
        return {'error': 'broker ' + brokerId + ' not found.'}

def create_entity(brokerIds, entity):
    brokers = utils.get_brokers_connections()
    print(brokers)
    for brokerId in brokerIds:
        if brokerId in brokers:
            brokers[brokerId].create_entity(entity)
        else:
            return {'error': 'broker ' + brokerId + ' not found.'}
    r = req.post("http://localhost:3000/clusters",
                data = dict(
                    brokers = brokerIds,
                    entity_id = entity['id'],
                    entity_type = entity['type']
                    )
                )
    if r.status_code == 200:
        return {"brokers": brokerIds }, 200
    else:
        return dict(error="cluster didn't created", message=r.text), 500
    

def delete_entity(brokerIds, _type, _id):
    brokers = utils.get_brokers_connections()
    print(brokers)
    for brokerId in brokerIds:
        if brokerId in brokers:
            brokers[brokerId].delete_entity( _type, _id)
        else:
            return {'error': 'broker ' + brokerId + ' not found.'}

    r = req.delete("http://localhost:3000/clusters",
            params = {
                "brokers[]" : brokerIds,
                "entity_id" : _id,
                "entity_type" : _type
                }
            )
    if r.status_code != 200:
        return dict(error="cluster didn't deleted", message=r.text), 500
    else:
        return r.json(), 200