from swagger_server.services import platforms_service, associations_service, entities_service
from swagger_server.middlewares import *

from swagger_server import utils

def list_associations(brokerId, entity_type = None, entity_id = None, device_type = None, device_id = None):
    brokers = utils.get_brokers_connections()
    # from id,broker in brokers.items():
    #     print(id, " - ", broker)
    print(brokers)
    associations = []
    if brokerId in brokers:
        associations = list(map(lambda assoc: assoc.to_dict(), brokers[brokerId].list_associations(entity_type = entity_type, entity_id = entity_id, device_type = device_type, device_id = device_id)))
    return associations

def get_association_by_id(brokerId, _id):
    brokers = utils.get_brokers_connections()
    print(brokers)
    if brokerId in brokers:
        return brokers[brokerId].get_association(_id)
    else:
        return {'error': 'broker ' + brokerId + ' not found.'}

def update_association(brokerId, association):
    brokers = utils.get_brokers_connections()
    print(brokers)
    if brokerId in brokers:
        return brokers[brokerId].update_association(association)
    else:
        return {'error': 'broker ' + brokerId + ' not found.'}

def create_association(brokerId, association):
    brokers = utils.get_brokers_connections()
    print(brokers)
    if brokerId in brokers:
        return brokers[brokerId].create_association(association)
    else:
        return {'error': 'broker ' + brokerId + ' not found.'}

def delete_association(brokerId, _id):
    brokers = utils.get_brokers_connections()
    print(brokers)
    if brokerId in brokers:
        return brokers[brokerId].delete_association(_id)
    else:
        return {'error': 'broker ' + brokerId + ' not found.'}