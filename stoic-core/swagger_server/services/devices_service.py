from swagger_server.services import platforms_service
from swagger_server.middlewares import *

from swagger_server import utils

from datetime import datetime

def list_devices(brokerId = None):
    brokers = utils.get_brokers_connections()
    # from id,broker in brokers.items():
    #     print(id, " - ", broker)
    print(brokers)
    devices = {}
    if not brokerId:
        for key, broker in brokers.items():
            devices[key] = list(map(lambda dev: dev.to_dict(), broker.list_devices()))
    else:
        devices[brokerId] = list(map(lambda dev: dev.to_dict(), brokers[brokerId].list_devices()))
    return devices

def get_device_by_id(brokerId, _type, _id):
    brokers = utils.get_brokers_connections()
    print(brokers)
    if brokerId in brokers:
        return brokers[brokerId].get_device(_type, _id)
    else:
        return {'error': 'broker ' + brokerId + ' not found.'}

def update_device(brokerId, device):
    brokers = utils.get_brokers_connections()
    print(brokers)
    if brokerId in brokers:
        return brokers[brokerId].update_device(device)
    else:
        return {'error': 'broker ' + brokerId + ' not found.'}

def create_device(brokerId, device):
    brokers = utils.get_brokers_connections()
    print(brokers)
    if brokerId in brokers:
        return brokers[brokerId].create_device(device)
    else:
        return {'error': 'broker ' + brokerId + ' not found.'}

def delete_device(brokerId, _type, _id):
    brokers = utils.get_brokers_connections()
    print(brokers)
    if brokerId in brokers:
        return brokers[brokerId].delete_device({'type': _type, 'id': _id})
    else:
        return {'error': 'broker ' + brokerId + ' not found.'}