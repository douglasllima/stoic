from swagger_server.services import platforms_service
from swagger_server.middlewares import *

from swagger_server import utils

def list_tasks(brokerId, entity_type, entity_id):
    brokers = utils.get_brokers_connections()
    # from id,broker in brokers.items():
    #     print(id, " - ", broker)
    print(brokers)
    tasks = {}
    if not brokerId:
        for key, broker in brokers.items():
            try:
                tasks[key]
            except:
                tasks[key] = []
            tasks[key] = broker.list_tasks(entity_type, entity_id)
    else:
        tasks[brokerId] = brokers[brokerId].list_tasks(entity_type, entity_id)
    return tasks

def get_task_by_id(brokerId, _id):
    brokers = utils.get_brokers_connections()
    print(brokers)
    if brokerId in brokers:
        return brokers[brokerId].get_task(_id)
    else:
        return {'error': 'broker ' + brokerId + ' not found.'}

def update_task(brokerId, taskId, attrs):
    brokers = utils.get_brokers_connections()
    print(brokers)
    if brokerId in brokers:
        return brokers[brokerId].update_task(taskId, attrs)
    else:
        return {'error': 'broker ' + brokerId + ' not found.'}

def create_task(brokerId, task):
    brokers = utils.get_brokers_connections()
    print(brokers)
    if brokerId in brokers:
        return brokers[brokerId].create_task(task)
    else:
        return {'error': 'broker ' + brokerId + ' not found.'}

def delete_task(brokerId, _id):
    brokers = utils.get_brokers_connections()
    print(brokers)
    if brokerId in brokers:
        return brokers[brokerId].delete_task(_id)
    else:
        return {'error': 'broker ' + brokerId + ' not found.'}