import re

def expEval(expression,id):
    # [a-zA-Z:$]+[=]{2}[0-9]+[.]?[0-9]*[.]{2}[0-9]+[.]?[0-9]*       attr==i..f
    # [a-zA-Z:$]+[!][=][0-9]+[.]?[0-9]*[.]{2}[0-9]+[.]?[0-9]*       attr!=i..f
    # [a-zA-Z:$]+[=]{2}([A-Za-z0-9.]+[,]*)+                              color==black,red,5,4,3.4
    # [a-zA-Z:$][!][=]{2}([A-Za-z0-9.]+[,]*)+                               color!=black,red,5,4,3.4
    rangesEqual = re.findall(r'[^;][A-Za-z]{1}[A-Za-z0-9]+[=]{2}[0-9]+[.]?[0-9]*[.]{2}[0-9]+[.]?[0-9]*', expression)
    print(rangesEqual)
    for range in rangesEqual:
        terms = range.split("==")
        tokens = terms[1].split('..')
        newRange = terms[0] + ">=" + tokens[0] + " AND " + terms[0] + "<=" + tokens[1]
        expression = expression.replace(range, newRange)
        print(newRange)
    print(expression)
    rangesInequal = re.findall(r'[^;][A-Za-z]{1}[A-Za-z0-9]+!=[0-9]+[.]?[0-9]*[.]{2}[0-9]+[.]?[0-9]*', expression)
    for range in rangesInequal:
        terms = range.split("!=")
        tokens = terms[1].split('..')
        newRange = terms[0] + ">=" + tokens[0] + " AND " + terms[0] + "<=" + tokens[1]
        expression = expression.replace(range, newRange)
        print(newRange)
    listsEqual = re.findall(r"[^;][A-Za-z]{1}[A-Za-z0-9]+[=]{2}(([0-9]+([.][0-9]+)?|[\"][A-Za-z0-9]*[\"]|['][A-Za-z0-9]*[']|[A-Za-z0-9]*)[,]?)+", expression)
    for ls in listsEqual:
        terms = range.split("==")
        tokens = terms[1].split(',')
        newRange = terms[0] + ">=" + tokens[0] + " AND " + terms[0] + "<=" + tokens[1]
        expression = expression.replace(range, newRange)
        print(newRange)
    listsInequal = re.findall(r'[^;][A-Za-z]{1}[A-Za-z0-9]+!=(([0-9]+([.][0-9]+)?|["][A-Za-z0-9]*["]|[\'][A-Za-z0-9]*[\']|[A-Za-z0-9]*)[,]?)+', expression)
    regex = re.findall(r'[^;][A-Za-z]{1}[A-Za-z0-9]+~=[^;]+)', expression)
    print(rangesInequal)
    print(listsEqual)
    print(listsInequal)
