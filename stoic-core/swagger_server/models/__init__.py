# coding: utf-8

# flake8: noqa
from __future__ import absolute_import
# import models into model package
from swagger_server.models.body import Body
from swagger_server.models.body1 import Body1
from swagger_server.models.body2 import Body2
from swagger_server.models.body3 import Body3
from swagger_server.models.body4 import Body4
from swagger_server.models.body5 import Body5
from swagger_server.models.body6 import Body6
from swagger_server.models.body7 import Body7
from swagger_server.models.body8 import Body8
from swagger_server.models.body9 import Body9
from swagger_server.models.inline_response200 import InlineResponse200
from swagger_server.models.inline_response2001 import InlineResponse2001
from swagger_server.models.inline_response2002 import InlineResponse2002
from swagger_server.models.inline_response2003 import InlineResponse2003
