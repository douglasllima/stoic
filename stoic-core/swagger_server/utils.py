from flask import current_app

from swagger_server.services import platforms_service
from swagger_server.middlewares import *

def get_brokers_connections(invalidate = False, app = current_app):
    if invalidate:
        brokers = {}
        p = platforms_service.list_brokers()
        for broker in p:
            try:
                _type = broker['type']
                if _type == 'fiware':
                    print("Fiware")
                    mw = fiware_middleware
                elif _type == 'watson':
                    print("Watson")
                    mw = watson_middleware
                brokers[broker['id']] = mw(broker, None)
                print("End broker")
            except Exception as e:
                print(e)        
        app.config['brokers'] = brokers
        return brokers
    if 'brokers' not in current_app.config:
        return get_brokers_connections(True)
    else:
        return current_app.config['brokers']
    # try:
    #     brokers
    # except:
    #     return update_brokers(True)
    # return brokers
