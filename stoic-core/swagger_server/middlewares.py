#from .base_middleware import BaseMiddleware
import wiotp.sdk.application
import wiotp.sdk.api
from wiotp.sdk.exceptions import ApiException

from wiotp.sdk.api.state.things import Things

from cloudant.client import Cloudant
from cloudant.error import CloudantException
from cloudant.result import Result, ResultByKey
from cloudant.query import Query
from cloudant.document import Document

import pprint
import json
import base64

import pystache
import requests

from datetime import datetime
import traceback

from colorama import Fore, Back, Style
from .elements import *

from pymongo import MongoClient
from bson.objectid import ObjectId

from urllib.parse import quote

import threading

import copy

# def load_middleware(mw):
#     module = __import__('swagger_server.middlewares')
#     module = getattr(module, 'middlewares')
#     return getattr(module, str(mw)+'_middleware')


def load_middleware(path, mw):
    module = __import__(path)
    module = getattr(module, 'middlewares')
    return getattr(module, str(mw)+'_middleware')


TAG = "DOUGLAS"

# class watson_middleware(BaseMiddleware):


class watson_middleware(object):
    def __init__(self, broker, parent, cloudant=None):
        self.parent = parent
        if cloudant:
            self.cloudant = cloudant
        self.cloudant = {
            'user': "6c505913-4696-4168-b63c-4354e4b13730-bluemix",
            'password': "ec07709b77140b43000189bf4c2ba78db446798860c55bfa80f19b7c2cc84aa3",
            'url': "https://6c505913-4696-4168-b63c-4354e4b13730-bluemix:ec07709b77140b43000189bf4c2ba78db446798860c55bfa80f19b7c2cc84aa3@6c505913-4696-4168-b63c-4354e4b13730-bluemix.cloudantnosqldb.appdomain.cloud",
            'db_entities': 'stoic_entities',
            'db_associations': 'stoic_associations',
            'db_actions': 'stoic_actions',
            'db_triggers': 'stoic_triggers',
            'db_tasks': 'stoic_tasks'
        }
        self.cloudant_client = Cloudant(
            self.cloudant['user'], self.cloudant['password'], url=self.cloudant['url'])
        self.cloudant_client.connect()
        self.db_entities = self.cloudant_client.create_database(
            self.cloudant['db_entities'])
        self.db_associations = self.cloudant_client.create_database(
            self.cloudant['db_associations'])
        self.db_actions = self.cloudant_client.create_database(
            self.cloudant['db_actions'])
        self.db_triggers = self.cloudant_client.create_database(
            self.cloudant['db_triggers'])
        self.db_tasks = self.cloudant_client.create_database(
            self.cloudant['db_tasks'])
        self.broker = broker
        self.app_client = wiotp.sdk.application.ApplicationClient(broker)
        self.app_client.connect()
        self.app_client.deviceEventCallback = self.callbacks
        self.app_client.subscribeToDeviceEvents()
        self.db_historical_entity = {}

        entity_changes = threading.Thread(target=self.on_entity_change)
        entity_changes.start()

        self.historical_entity = False

    def on_entity_change(self):
        changes = self.db_entities.changes(feed='continuous', since='now', include_docs=True)
        for change in changes:
            entity = change['doc']
            type_id = entity.pop('_id')
            entity['type'] = type_id.split(':')[0]
            entity['id'] = type_id.split(':',1)[1]
            entity.pop('_rev')

            tasks = self.list_tasks(entity['type'], entity['id'])
            self.persist_entity(entity)
            for task in tasks:
                tk = Task.build(task)
                tk.bind_entity(entity)
                tk.fire()

    def watson2device(self, wdevice):
        d = {
            'id': wdevice['deviceId'],
            'type': wdevice['typeId']
        }
        d = Device(_type=wdevice['typeId'], _id=wdevice['deviceId'])
        try:
            d.description = wdevice['deviceInfo']['description']
        except KeyError:
            pass
        return d
    
    @staticmethod
    def get_schema():
        return {
        "title": "Watson IoT Platform",
        "type": "object",
        "properties": {
            "auth": {
                "title": "Authentication Credentials",
                "type": "object",
                "properties": {
                    "key": {
                        "title":"Key",
                        "type": "string"
                    },
                    "token": {
                        "title":"Token Auth",
                        "type": "string"
                    }
                },
                "required": ["key", "token"]
            },
            "mqtt": {
            "title":"MQTT Definitions",
            "type":"object",
            "properties": {
                "port": {
                "title":"Port",
                "type":"integer"
                },
                "transport": {
                "title":"Transport Layer",
                "enum": ["tcp","tls"],
                "type":"string"
                },
                "cleanStart": {
                "title":"Clean Start",
                "type": "boolean"
                },
                "sessionExpiry": {
                "title":"Session Expiry",
                "type":"number"
                },
                "keepAlive": {
                "title":"Keep Alive",
                "type":"number"
                }
            },
            "required":["port","transport","cleanStart","sessionExpiry","keepAlive"]
            },
            "watson": {
            "title":"Watson IoT",
            "type":"object",
            "properties": {
                "org": {
                "title":"Organisation",
                "type":"string"
                }
            },
            "required":["org"]
            },
            "cloudant": {
            "title":"Cloudant",
            "type":"object",
            "properties": {
                "user": {
                "title":"Username",
                "type":"string"
                },
                "password": {
                "title":"Password",
                "type":"string"
                },
                "url": {
                    "title":"URL Connection",
                    "type": "string",
                    "format": "url"
                },
                "db_entities": {
                "title":"Entities Database",
                "type":"string"
                },
                "db_associations": {
                "title":"Associations Database",
                "type":"string"
                },
                "db_actions": {
                "title":"Actions Database",
                "type":"string"
                },
                "db_triggers": {
                "title":"Triggers Database",
                "type":"string"
                },
                "db_tasks": {
                "title":"Tasks Database",
                "type":"string"
                }
            },
            "required":[
                "user",
                "password",
                "url",
                "db_entities",
                "db_associations",
                "db_actions",
                "db_triggers",
                "db_tasks"
            ]
            }
        },
        "required": ["url", "auth", "mqtt","watson"]
    }

    def device2watson(self, device):
        wdevice = {
            'deviceId': device['id'],
            'typeId': device['type'],
            "authToken": "mytoken12345",
            'deviceInfo': {}
        }
        try:
            wdevice['deviceInfo']['description'] = device['description']
        except KeyError:
            pass
        return wdevice

    def list_devices(self):
        devices = []
        for d in self.app_client.registry.devices:
            w2d = self.watson2device(d)
            devices.append(w2d)
        return devices

    def get_device(self, _type, id):
        self.app_client.registry.devices.typeId = _type
        d = self.app_client.registry.devices[id]
        self.app_client.registry.devices.typeId = None
        return self.watson2device(d)

    def create_device(self, device):
        log("create_device()", self.create_type({'id': device['type']}))
        w = self.device2watson(device)
        r = self.app_client.registry.devices.create(w)
        return r['success']

    def update_device(self, device):
        pass

    def delete_device(self, device):
        w = self.device2watson(device)
        del w['deviceInfo']
        del w['authToken']
        r = self.app_client.registry.devices.delete(w)
        return r[0]['success']

    def create_type(self, _type):
        # Retrieve a devicetype from the registry, if the devicetype doesn't exist create it
        deviceType = None
        try:
            deviceType = self.app_client.registry.devicetypes[_type['id']]
            return deviceType
        except KeyError:
            log("create_type", "Device Type %s did not exist, creating it now ..." % (
                _type['id']))
            try:
                deviceType = self.app_client.registry.devicetypes.create(_type)
                return deviceType
            except ApiException as ae:
                log(TAG, ae.message)

    def list_entities(self):
        entities = []
        for entity in self.db_entities:
            ent = dict(entity)
            id_type = ent['_id'].split(':', 1)
            log(TAG, ent)
            ent['id'] = id_type[1]
            ent['type'] = id_type[0]
            del ent['_rev']
            del ent['_id']
            entities.append(Entity.build(ent))
        return entities

    def create_entity(self, entity):
        entity = entity.to_dict() if isinstance(entity, Entity) else entity
        entity = copy.deepcopy(entity)
        entity['_id'] = "{0}:{1}".format(entity['type'], entity['id'])
        del entity['id']
        del entity['type']
        self.db_entities.create_document(entity)
        self.persist_entity(entity)
        return True

    def get_entity(self, _type, id):
        # return Result(self.db_entities.all_docs["{0}:{1}".format(_type,id)])
        sel = '{0}:{1}'.format(_type, id)
        query = Query(self.db_entities, selector={'_id': {'$eq': sel}})
        entity = dict(query()['docs'][0])
        id_type = entity['_id'].split(':', 1)
        entity['id'] = id_type[1]
        entity['type'] = id_type[0]
        del entity['_id']
        del entity['_rev']
        return Entity.build(entity)

    def update_entity(self, _type, _id, attrs):
        with Document(self.db_entities, _type+':'+_id) as entity:
            for attr, value in attrs.items():
                entity[attr] = value
            entity = dict(entity)
            id_type = entity['_id'].split(':', 1)
            entity['id'] = id_type[1]
            entity['type'] = id_type[0]
            del entity['_id']
            del entity['_rev']
            self.persist_entity(entity)

    def delete_entity(self, _type, id):
        try:
            entity = self.db_entities[_type+':'+id]
            entity.delete()
        except KeyError:
            print("Entity don't exists")
        return True

    def list_associations(self, entity_type=None, entity_id=None, device_type=None, device_id=None):
        sel = {}
        associations = []
        if entity_type and entity_id:
            sel = {
                'entity_type': entity_type,
                'entity_id': entity_id
            }
        if device_type and device_id:
            if not sel:
                sel = {}
            sel.update({
                'device_type': {'$eq': device_type},
                'device_id': {'$eq': device_id}
            })
        if sel:
            log(TAG, sel)
            query = Query(self.db_associations, selector=sel)
            for assoc in query()['docs']:
                del assoc['_rev']
                assoc['id'] = assoc['_id']
                a = dict(assoc)
                a = Association.build(a)
                associations.append(a)
        else:
            for association in self.db_associations:
                assoc = dict(association)
                assoc['id'] = assoc['_id']
                del assoc['_rev']
                assoc = Association.build(assoc)
                associations.append(assoc)
        return associations

    def create_association(self, assoc):
        assoc = assoc.to_dict() if isinstance(assoc, Association) else assoc
        del assoc['id']
        r = self.db_associations.create_document(assoc)
        if r:
            log("create_association()", "Created", "info")
            return r['_id']
        else:
            return None

    def get_association(self, _id):
        query = Query(self.db_associations, selector={'_id': {'$eq': _id}})
        association = dict(query()['docs'][0])
        del association['_rev']
        return Association.build(association)

    def update_association(self, _id, attrs):
        association = self.db_associations[_id]
        association.update(attrs)
        association.save()

    def delete_association(self, _id):
        self.db_associations[_id].delete()

    def list_actions(self):
        actions = []
        for action in self.db_actions:
            act = dict(action)
            act['id'] = act['_id']
            del act['_rev']
            del act['_id']
            actions.append(Action.build(act))
        return actions

    def create_action(self, action):
        action = action.to_dict() if isinstance(action, Action) else action
        del action['id']
        log("create_action()", "Created", "info")
        r = self.db_actions.create_document(action)
        return r['_id']

    def get_action(self, _id):
        query = Query(self.db_actions, selector={'_id': {'$eq': _id}})
        action = dict(query()['docs'][0])
        del action['_rev']
        return Action.build(action)

    def update_action(self, _id, attrs):
        action = self.db_actions[_id]
        action.update(attrs)
        action.save()

    def delete_action(self, _id):
        self.db_actions[_id].delete()

    def invoke_action(self, action, vars):
        log(TAG, vars)
        log(TAG, pystache.render(action['body'], vars))
        try:
            r = requests.request(method=action['method'], url=action['target'], headers=action['headers'],
                                 params=action['params'], data=pystache.render(action['body'], vars))
            return r.raw
        except requests.exceptions.RequestException as e:
            log(TAG, "Error connection")
            log(TAG, e)

    def list_triggers(self):
        triggers = []
        for trigger in self.db_triggers:
            trigger = dict(trigger)
            trigger['id'] = trigger['_id']
            del trigger['_rev']
            del trigger['_id']
            triggers.append(Trigger.build(trigger))
        return triggers

    def create_trigger(self, trigger):
        trigger = trigger.to_dict() if isinstance(trigger, Trigger) else trigger
        log("create_trigger()", "Criado", "info")
        r = self.db_triggers.create_document(trigger)
        return r['_id']

    def get_trigger(self, _id):
        query = Query(self.db_triggers, selector={'_id': {'$eq': _id}})
        trigger = dict(query()['docs'][0])
        del trigger['_rev']
        return Trigger.build(trigger)

    def update_trigger(self, _id, attrs):
        trigger = self.db_triggers[_id]
        trigger.update(attrs)
        trigger.save()

    def delete_trigger(self, _id):
        self.db_triggers[_id].delete()

    def list_tasks(self, entity_type=None, entity_id=None, entity_idPattern=None):
        sel = {}
        tasks = []
        if entity_type and entity_id:
            # sel = {
            #     'trigger.entities.type': entity_type,
            #     'trigger.entities.id': entity_id
            # }
            match = {}
            if entity_type:
                match["type"] = {"$eq": entity_type}
            if entity_id:
                match["id"] = {"$eq": entity_id}
            elif entity_idPattern:
                match["id"] = {"$regex": entity_idPattern}
            sel = {
                'trigger.entities': {
                    "$elemMatch" : match
                }
            }
        if sel:
            log(TAG, sel)
            query = Query(self.db_tasks, selector=sel)
            for task in query()['docs']:
                t = dict(task)
                t['id'] = t.pop('_id')
                del t['_rev']
                tk = Task.build(t).to_dict()
                tasks.append(tk)
        else:
            for task in self.db_tasks:
                t = dict(task)
                t['id'] = t.pop('_id')
                del t['_rev']
                # tk = Task.build(t).to_dict()
                tk = t
                tasks.append(tk)
        return tasks

    def create_task(self, task):
        r = self.db_tasks.create_document(task)
        return r['_id']

    def get_task(self, _id):
        query = Query(self.db_tasks, selector={'_id': {'$eq': _id}})
        task = dict(query()['docs'][0])
        del task['_rev']
        return task
        #return Task.build(task)

    def update_task(self, _id, attrs):
        task = self.db_tasks[_id]
        task.update(attrs)
        task.save()

    def delete_task(self, _id):
        # query = Query(self.db_tasks, selector={'_id': {'$eq': _id}})
        # task = query()['docs'][0]
        # task.delete()
        self.db_tasks[_id].delete()

    def create_historical_device(self, historical):
        has_historical = False
        for conn in self.app_client.dsc:
            if conn['type'] != 'cloudant':
                continue
            for rule in conn.rules:
                try:
                    if rule['type'] == 'event' and rule['selector']['deviceType'] == '*' and rule['selector']['eventId'] == '*':
                        has_historical = True
                        break
                except KeyError as ke:
                    log(TAG, ke)
        if has_historical:
            log(TAG, "Historical já existente")
        else:
            log(TAG, "Historical a ser criado")
            # Configure the binding
            historical = {
                "name": "mycloudant",
                "type": "cloudant",
                "description": "uma descrição",
                "timezone": 'America/Sao_Paulo',
                "credentials": {
                    "host": "https://6c505913-4696-4168-b63c-4354e4b13730-bluemix:ec07709b77140b43000189bf4c2ba78db446798860c55bfa80f19b7c2cc84aa3@6c505913-4696-4168-b63c-4354e4b13730-bluemix.cloudantnosqldb.appdomain.cloud",
                    "port": 443,
                    "username": "6c505913-4696-4168-b63c-4354e4b13730-bluemix",
                    "password": "ec07709b77140b43000189bf4c2ba78db446798860c55bfa80f19b7c2cc84aa3"
                }
            }

            service = self.app_client.serviceBindings.create(historical)
            # Set up the connector
            connector = self.app_client.dsc.create(name="connector1", type="cloudant", serviceId=service.id,
                                                   description="Uma descrição", timezone=historical['timezone'], enabled=True)
            # Set up destinations
            connector.destinations.create(
                name="events", bucketInterval="MONTH")
            #connector.destinations.create(name="state", bucketInterval="MONTH")
            # Set up rules
            rule = connector.rules.createEventRule(
                name="allevents", destinationName="events", typeId="*", eventId="*", description="Uma regra", enabled=True)
            #rule2 = connector.rules.createStateRule(name="allstate", destinationName="state", logicalInterfaceId="*")
            log(TAG, "Historical criado")

    def create_historical_entity(self, historical):
        self.historical_entity = historical['activate']

    def persist_entity(self, entity):
        entity = entity.to_dict() if isinstance(entity, Entity) else entity
        if not self.historical_entity:
            return
        log(TAG, "Persiste "+"stoic_"+entity['type']+'_'+entity['id'])
        if not "stoic_"+entity['type']+'_'+entity['id'] in self.db_historical_entity:
            db = self.db_historical_entity["stoic_"+entity['type']+'_'+entity['id']
                                           ] = self.cloudant_client.create_database("stoic_"+entity['type']+'_'+entity['id'])
        else:
            db = self.db_historical_entity["stoic_" +
                                           entity['type']+'_'+entity['id']]
        del entity['id']
        del entity['type']
        entity['timestamp'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        db.create_document(entity)  # BUG?

    def callbacks(self, event):
        str = "%s event '%s' received from device [%s]: %s"
        log(TAG, str % (event.format, event.eventId,
                        event.device, json.dumps(event.data)))
        typeId = event.device.split(':')[0]
        deviceId = event.device.split(':')[1]
        if event.eventId == 'stoic':
            associations = self.list_associations(
                device_type=typeId, device_id=deviceId)
            #log(TAG, associations)
            for assoc in associations:
                attrs = {}
                for source, dest in assoc['mapping'].items():
                    try:
                        attrs[dest['name']] = {
                            'value': event.data[source],
                            'type': dest['type']
                        }
                    except KeyError:
                        pass
                if not assoc['entity_type'] + ':' + assoc['entity_id'] in self.db_entities:
                    entity = {
                        'id': assoc['entity_id'],
                        'type': assoc['entity_type']
                    }
                    entity.update(attrs)
                    self.create_entity(entity)
                else:
                    self.update_entity(
                        assoc['entity_type'], assoc['entity_id'], attrs)
                entity = self.get_entity(
                    assoc['entity_type'], assoc['entity_id'])
                # tasks = self.list_tasks(
                #     assoc['entity_type'], assoc['entity_id'])
                # self.persist_entity(entity)
                # for task in tasks:
                #     action = self.get_action(task['action_id'])
                #     trigger = self.get_trigger(task['trigger_id'])
                #     entity = self.get_entity(
                #         trigger.entity_type, trigger.entity_id)
                #     trigger.bind_entity(entity)
                #     if trigger.evaluate():
                #         attrs = entity.attrs
                #         values = {}
                #         for k, v in attrs.items():
                #             values[k] = v['value']
                #         action.invoke(values)


class fiware_middleware(object):
    def __init__(self, broker, parent):
        self.parent = parent
        #self.iota = broker['iota']
        self.iota = broker['iota']
        #self.orion = broker['orion']
        self.orion = broker['orion']
        self.cygnus = broker['cygnus']
        self.mongodb = broker['mongodb']
        self.historical_entity = False
        self.mongo_client = MongoClient(broker['mongodb']['url'])
        self.db_stoic = self.mongo_client['stoic']
        self.db_actions = self.db_stoic['actions']
        self.db_triggers = self.db_stoic['triggers']
        self.db_tasks = self.db_stoic['tasks']
        self.historical_entity = broker['historical']['activate']
        if self.historical_entity:
            options = {
                "subject": {
                    "entities": [
                        {
                            "idPattern": ".*"
                        }
                    ]
                },
                "notification": {
                    "http": {
                        "url": self.cygnus['url'] + self.cygnus['notify_root']
                    },
                    "attrsFormat": "legacy"
                }
            }
            options_headers = {
                "Content-Type": "application/json"
            }
            options_headers.update(self.iota['headers'])
            r = requests.get(
                url=self.orion['url'] + "/v2/subscriptions/",
                data=json.dumps(options),
                headers=options_headers
            )
            if len(r.json()) > 0:
                log("FIWARE.__init__()", "Assinatura Cygnus pre-existente.", 'info')
            else:
                persiste_headers = {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Accept': 'application/json'
                }
                persiste_headers.update(self.iota['headers'])
                persiste = {
                    "description": "Notify Cygnus of all context changes",
                    "subject": {
                        "entities": [
                            {
                                "idPattern": ".*"
                            }
                        ]
                    },
                    "notification": {
                        "http": {
                            "url": self.cygnus['url'] + self.cygnus['notify_root']
                        },
                        "attrsFormat": "legacy"
                    },
                    "throttling": 5
                }
                persiste = data = json.dumps(persiste)
                r = requests.request(
                    "POST", url=self.orion['url'] + "/v2/subscriptions", data=persiste, headers=persiste_headers)
                if r.status_code == 201:
                    log("FIWARE.__init__()", "Cygnus assinado com sucesso", 'info')
                else:
                    log("FIWARE.__init__()", r.text, 'error')

    def list_devices(self):
        headers = {
            'User-Agent': "STOIC/0.1",
            'Accept': "*/*",
            'Host': "localhost:4061",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }
        headers.update(self.iota['headers'])
        try:
            response = requests.request(
                "GET", self.iota['url'] + '/devices', headers=headers)
        except:
            return []

        #log('RESPONSE', response.text, 'info')
        devices = []
        fw_devices = response.json()['devices']
        for dev in fw_devices:
            if not "_" in dev['device_id']:
                dev['device_id'] = 'generic_' + dev['device_id']
            devices.append(Device(dev['device_id'].split('_', 1)[
                           0], dev['device_id'].split('_', 1)[1]))
        return devices

    def get_device(self, _type, _id):
        headers = {
            'User-Agent': "STOIC/0.1",
            'Accept': "*/*",
            'Host': "localhost:4061",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }
        headers.update(self.iota['headers'])
        if _type in ('generic', ''):
            dev = _id
        else:
            dev = _type + '_' + _id
        response = requests.request(
            "GET", self.iota['url'] + '/devices/' + dev, headers=headers)

        log('RESPONSE', response.text, 'info')
        device = response.json()
        if response.status_code != 200:
            return None
        return Device(_type, _id)

    def create_device(self, device):

        payload = {
            "devices": [
                {
                    "device_id":   device['type'] + '_' + device['id'],
                    "transport":   "HTTP"
                }
            ]
        }
        headers = {
            'Content-Type': "application/json",
            'User-Agent': "STOIC/0.1",
            'Accept': "*/*",
            'Cache-Control': "no-cache",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }
        headers.update(self.iota['headers'])
        response = requests.request(
            "POST", self.iota['url'] + '/devices', data=json.dumps(payload), headers=headers)

        if response.status_code == 201:
            log("create_device()", response.status_code, 'info')
            log("create_device()", response.text, 'info')
            return True
        else:
            log("create_device()", response.status_code, 'error')
            log("create_device()", response.text, 'error')
            return False

    def update_device(self, device):
        pass

    def delete_device(self, device):
        headers = self.iota['headers']
        if device['type'] in ('generic', ''):
            dev = device['id']
        else:
            dev = device['type'] + '_' + device['id']

        response = requests.request(
            "DELETE", self.iota['url'] + '/devices/' + dev, headers=headers)

        if response.status_code == 204:
            log("delete_device()", "Deleted", 'info')
            return True
        else:
            log("delete_device()", response.text, 'error')
            return False

    def list_entities(self):
        headers = {
            'cache-control': "no-cache"
        }
        headers.update(self.iota['headers'])
        response = requests.request(
            "GET", url=self.orion['url'] + '/v2/entities', headers=headers)

        log("list_entities()", response.status_code, 'info')
        log("list_entities()", response.json(), 'info')

        entities = []

        for ent in response.json():
            entities.append(Entity.build(ent))

        return entities

    def create_entity(self, entity):
        entity = entity.to_dict() if isinstance(entity, Entity) else entity
        entity = json.dumps(entity)
        headers = {
            'Accept-Language': "application/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }
        headers.update(self.iota['headers'])

        response = requests.request(
            "POST", url=self.orion['url'] + '/v2/entities', data=entity, headers=headers)

        if response.status_code in [201, 204]:
            log("create_entity()", response.status_code, 'info')
            log("create_entity()", response.text, 'info')
            return True
        else:
            log("create_entity()", response.status_code, 'error')
            log("create_entity()", response.text, 'error')
            return False

    def get_entity(self, _type, _id):
        url = "http://localhost:1026/v2/entities/urn:ngsi-ld:Store:001"

        headers = {
            'cache-control': "no-cache",
        }
        headers.update(self.iota['headers'])

        response = requests.request(
            "GET", self.orion['url'] + '/v2/entities/' + _id + '&type=' + _type, headers=headers)
        if response.status_code == 200:
            log("get_entity()", response.status_code, 'info')
            log("get_entity()", response.json(), 'info')
        else:
            log("get_entity()", response.status_code, 'error')
            log("get_entity()", response.json(), 'error')

        print(response.json())
        return response.json()

    def update_entity(self, _type, _id, attrs):
        headers = {
            'cache-control': "no-cache",
            'content-type': 'application/json'
        }
        headers.update(self.iota['headers'])
        route = '/v2/entities/{0}/attrs'.format(_id)
        params = {
            'type': _type
        }
        response = requests.request(
            "PUT", self.orion['url'] + route, headers=headers, data=json.dumps(attrs), params=params)
        if response.status_code == 204:
            log("update_entity()", response.status_code, 'info')
            log("update_entity()", response.text, 'info')
            return True
        else:
            log("update_entity()", response.status_code, 'error')
            log("update_entity()", response.text, 'error')
            return False

    def update_entity_attr(self, _type, _id, attr, value):
        r = requests.request(
            method='PUT', url=self.orion['url'] + '/v2/entities/' + _id + '/attrs/' + attr + '/value', params={'type': 'Room'})
        if r.status_code == 200:
            log("update_entity_attr()", "Updated", 'info')
            return True
        else:
            log("update_entity_attr()", r.text, 'error')
            return False

    def get_entity_attr(self, _type, _id, attr):
        r = requests.request(
            method='PUT', url=self.orion['url'] + '/v2/entities/' + _id + '/attrs/' + attr + '/value', params={'type': 'Room'})
        if r.status_code == 200:
            log("get_entity_attr()", "Got", "info")
            return True
        else:
            log("get_entity_attr()", r.text, "error")
            return False

    def delete_entity(self, _type, _id):
        headers = {
            'cache-control': "no-cache",
        }
        headers.update(self.iota['headers'])
        route = '/v2/entities/{0}'.format(_id, _type)
        params = {
            'type': _type
        }
        response = requests.request(
            "DELETE", self.orion['url'] + route, headers=headers, params=params)
        if response.status_code == 204:
            log("delete_entity()", response.status_code, 'info')
            log("delete_entity()", response.text, 'info')
            return True
        else:
            log("delete_entity()", response.status_code, 'error')
            log("delete_entity()", response.text, 'error')
            return False

    def list_associations(self, entity_type=None, entity_id=None, device_type=None, device_id=None):
        headers = {
            'User-Agent': "STOIC/0.1",
            'Accept': "*/*",
            'Cache-Control': "no-cache",
            'Host': "localhost:4061",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive"
        }
        headers.update(self.iota['headers'])

        response = requests.request(
            "GET", self.iota['url'] + '/devices', headers=headers)

        print(response.text)
        associations = []
        fw_devices = response.json()['devices']
        for dev in fw_devices:
            assoc = Association(dev['device_id'], dev['device_id'].split('_')[
                                0], dev['device_id'].split('_', 1)[1], dev['entity_type'], dev['entity_name'], None)
            for attr in dev['attributes']:
                assoc.map(attr['object_id'], attr['name'], attr['type'])
            shouldAdd = True
            if entity_type != None and entity_type != assoc.entity_type:
                shouldAdd = False
            if entity_id != None and entity_id != assoc.entity_id:
                shouldAdd = False
            if shouldAdd:
                associations.append(assoc)
        return associations

    def create_association(self, assoc):
        assoc = assoc.to_dict() if isinstance(assoc, Association) else assoc
        import requests

        url = self.iota['url'] + ('/devices/%s_%s' %
                                  (assoc['device_type'], assoc['device_id']))

        attributes = []

        for key, val in assoc["mapping"].items():
            attributes.append({
                "object_id": key,
                "name": val["name"],
                "type": val["type"]
            })
        headers = {
            'Content-Type': "application/json"
        }
        headers.update(self.iota['headers'])
        payload = {
            "entity_name": assoc['entity_id'],
            "entity_type": assoc['entity_type'],
            "attributes": attributes
        }
        response = requests.request(
            "PUT", url, data=json.dumps(payload), headers=headers)
        if response.status_code == 204:
            log("create_association()", "Created", "info")
            return True
        else:
            log("create_association()", response.text, "error")
            return False

    def get_association(self, _id):  # _id --> device_id:object_id:name:type

        headers = {
            'fiware-service': self.iota['fiware-service'],
            'fiware-servicepath': self.iota['fiware-servicepath'],
            'User-Agent': "PostmanRuntime/7.19.0",
            'Accept': "*/*",
            'Cache-Control': "no-cache",
            'Host': "localhost:4061",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }

        response = requests.request(
            "GET", url=self.iota['url'] + "/devices/" + _id, headers=headers)
        dev = response.json()
        assoc = Association(None, dev['device_id'].split(
            '_')[0], dev['device_id'].split('_')[0], entity_type, entity_id, None)
        for attr in fw_devices['attributes']:
            assoc.map(attr['object_id'], attr['name'], attr['type'])

        print(response.text)
        return Association.build(assoc)

    def update_association(self, _id, attrs):
        headers = {
            'fiware-service': self.iota['fiware-service'],
            'fiware-servicepath': self.iota['fiware-servicepath'],
            'User-Agent': "PostmanRuntime/7.19.0",
            'Accept': "*/*",
            'Cache-Control': "no-cache",
            'Host': "localhost:4061",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }

        response = requests.request(
            "GET", url=self.iota['url'] + "/devices/" + _id, headers=headers)
        dev = response.json()
        attributes = dev['attributes']
        for attr in attrs:
            attributes.append(
                {'object_id': attr['object_id'], 'name': attr['name'], 'type': attr['type']})
        response = requests.request(
            "PUT", url=self.iota['url'] + "/devices/" + _id, data=payload, headers=headers)

    def delete_association(self, _id):

        payload = {'attributes': []}
        headers = {
            'fiware-service': self.iota['fiware-service'],
            'fiware-servicepath': self.iota['fiware-servicepath'],
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = requests.request(
            "PUT", url=self.iota['url'] + "/devices/" + _id, data=payload, headers=headers)

        print(response.text)

    def list_actions(self):
        actions = []
        for action in self.db_actions.find():
            act = dict(action)
            act['id'] = str(act['_id'])
            del act['_id']
            actions.append(Action.build(act))
        return actions

    def create_action(self, action):
        action = action.to_dict() if isinstance(action, Action) else action
        del action['id']
        inserted = self.db_actions.insert_one(action)
        log('create_action()', action, 'info')
        return str(inserted.inserted_id)

    def get_action(self, _id):
        query = self.db_actions.find_one({'_id': ObjectId(_id)})
        action = dict(query)
        if query is None:
            log("get_action()", "Error: Action not found", "error")
            return None
        else:
            action['id'] = str(ObjectId(_id))
            del action['_id']
            return Action.build(action)

    def update_action(self, _id, attrs):
        query = {"_id": ObjectId(_id)}
        newvalues = {"$set": attrs}
        self.db_actions.update_one(query, newvalues)
        action = self.get_action(_id)
        tasks = self.db_tasks.find({'action_id': ObjectId(_id)})
        updated = True
        for task in tasks:
            if self.update_task(task['_id'], {'action_id': _id}) is None:
                updated = False
        if not updated:
            log("update_action()", "An error occured", 'error')
            return None
        else:
            log("update_action()", "Success", 'info')
            return action

    def delete_action(self, _id):
        tasks = self.db_tasks.find({'action_id': ObjectId(_id)})
        deleted = True
        for task in tasks:
            if self.delete_task(task['_id']) is None:
                deleted = False
        if deleted:
            log("delete_action()", "Deleted", "info")
            self.db_actions.delete_one({'_id': ObjectId(_id)})
        else:
            log("delete_action()", "An error occured", "error")

    def list_triggers(self):
        triggers = []
        for trigger in self.db_triggers.find():
            trigger = dict(trigger)
            trigger['id'] = str(trigger['_id'])
            del trigger['_id']
            triggers.append(Trigger.build(trigger))
        return triggers

    def create_trigger(self, trigger):
        trigger = trigger.to_dict() if isinstance(trigger, Trigger) else trigger
        return str(self.db_triggers.insert_one(trigger).inserted_id)

    def get_trigger(self, _id):
        query = self.db_triggers.find_one({'_id': ObjectId(_id)})
        trigger = dict(query)
        return Trigger.build(trigger)

    def update_trigger(self, _id, attrs):
        query = {"_id": _id}
        newvalues = {"$set": attrs}
        self.db_triggers.update_one(query, newvalues)
        trigger = self.get_trigger(_id)
        tasks = self.db_tasks.find({'trigger_id': ObjectId(_id)})
        updated = True
        for task in tasks:
            if self.update_task(task['_id'], {'trigger_id': _id}) is None:
                updated = False
        if not updated:
            log("update_trigger()", "An error occured", 'error')
            return None
        else:
            log("update_trigger()", "Success", 'info')
            return trigger

    def delete_trigger(self, _id):
        tasks = self.db_tasks.find({'trigger_id': ObjectId(_id)})
        for task in tasks:
            if self.delete_task(task['_id']) is None:
                deleted = False
        if deleted:
            log("delete_trigger()", "Deleted", "info")
            self.db_triggers.delete_one({'_id': ObjectId(_id)})
            return True
        else:
            log("delete_trigger()", "An error occured", "error")
            return False

    def list_tasks(self, entity_type=None, entity_id=None):
        sel = {}
        if entity_type:
            sel['entity_type'] = entity_type
        if entity_id:
            sel['entity_id'] = entity_id
        headers = {
            'Accept-Language': "application/json",
        }
        headers.update(self.iota['headers'])
        r = requests.request("GET", url=self.orion['url'] + "/v2/subscriptions",headers=headers)

        subs = r.json()
        tasks = []

        for sub in subs:
            task = sub
            task["trigger"] = task.pop("subject")
            task["action"] = task.pop("notification")
            
            try:
                task["trigger"]["condition"]["expression"]["q"]
            except KeyError:
                task["trigger"].pop("condition")
            else:
                task["trigger"]["condition"] = task["trigger"]["condition"]["expression"]["q"]
            
            try:
                task["action"]["notification"]["httpCustom"]["qs"]
            except KeyError:
                pass
            else:
                task["action"]["notification"]["httpCustom"]["params"] = task["action"]["notification"]["httpCustom"].pop("qs")
            tasks.append(task)
        if sel:
            _sel = dict(
                id=sel['entity_id'],
                type=sel['entity_type']
                )
            tasks = list(filter(lambda task: _sel in task["trigger"]["entities"], tasks))

        log(TAG, sel)
        return tasks

        """
        {
        "title": "Task",
        "type": "object",
        "required": [
            "trigger",
            "action"
        ],
        "additionalProperties": false,
        "properties": {
            "description": {
            "title": "Description",
            "type": "string"
            },
            "trigger": {
            "title": "Trigger",
            "type": "object",
            "properties": {
                "entities": {
                "title": "Entity",
                "type": "object",
                "oneOf": [
                    {
                    "required": [
                        "id",
                        "type"
                    ],
                    "additionalProperties": false,
                    "properties": {
                        "id": {
                        "title": "ID",
                        "type": "string"
                        },
                        "type": {
                        "title": "Type",
                        "type": "string"
                        }
                    }
                    },
                    {
                    "required": [
                        "idPattern",
                        "type"
                    ],
                    "additionalProperties": false,
                    "properties": {
                        "idPattern": {
                        "title": "ID Regex Pattern",
                        "type": "string"
                        },
                        "type": {
                        "title": "Type",
                        "type": "string"
                        }
                    }
                    }
                ]
                },
                "condition": {
                "title": "Condition",
                "type": "string"
                }
            }
            },
            "action": {
            "title": "Action",
            "type": "object",
            "oneOf": [
                {
                "required": [
                    "http"
                ],
                "additionalProperties": false,
                "properties": {
                    "http": {
                    "title": "Default Action",
                    "type": "object",
                    "properties": {
                        "url": {
                        "title": "Target URL",
                        "type": "string",
                        "format": "url"
                        }
                    }
                    }
                }
                },
                {
                "required": [
                    "httpCustom"
                ],
                "additionalProperties": false,
                "properties": {
                    "httpCustom": {
                    "title": "Custom Action",
                    "type": "object",
                    "properties": {
                        "url": {
                        "title": "Target URL",
                        "type": "string",
                        "format": "url"
                        },
                        "headers": {
                        "title": "HTTP Headers",
                        "type": "object"
                        },
                        "params": {
                        "title": "Query Params",
                        "type": "object"
                        },
                        "method": {
                        "title": "Method",
                        "type": "string",
                        "enum": [
                            "GET",
                            "POST",
                            "PUT",
                            "PATCH",
                            "DELETE"
                        ]
                        },
                        "payload": {
                        "title": "Payload",
                        "anyOf": [
                            {
                            "type": "object"
                            },
                            {
                            "type": "string"
                            },
                            {
                            "type": "number"
                            }
                        ]
                        }
                    }
                    }
                }
                }
            ]
            }
        }
        }
        """
        # query = self.db_tasks.find(sel)
        # for task in query:
        #     t = dict(task)
        #     t['id'] = str(t['_id'])
        #     del t['_id']
        #     tasks.append(Task.build(t))
        # return tasks

    def create_task(self, task):        
        task["subject"] = task.pop("trigger")
        task["notification"] = task.pop("action")
        
        task["subject"]["condition"] = dict(expression = { "q": task["subject"]["condition"] })

        try:
            task["action"]["notification"]["httpCustom"]["params"]
        except KeyError:
            pass
        else:
            task["action"]["notification"]["httpCustom"]["qs"] = task["action"]["notification"]["httpCustom"].pop("params")
        task["status"] = "active"

        headers = {'Content-Type': 'application/json'}
        headers.update(self.iota['headers'])
        r = requests.request(
            "POST", url=self.orion['url'] + "/v2/subscriptions", data=json.dumps(task), headers=headers)
        if r.status_code == 201:
            return {}
        else:
            log("create_task()", "An error ocurred: " + r.text)
            return {"error":"An error ocurred " + r.text}

    def get_task(self, _id):
        headers = {'accept': 'application/json'}
        headers.update(self.iota['headers'])
        r = requests.request("GET", url=self.orion['url'] + "/v2/subscriptions/" + _id, headers=headers)
        if r.status_code == 200:
            task = r.json()
            task["trigger"] = task.pop("subject")
            task["action"] = task.pop("notification")
            
            try:
                task["trigger"]["condition"]["q"]
            except KeyError:
                task["trigger"].pop("condition")
            else:
                task["trigger"]["condition"] = task["trigger"]["condition"]["q"]
            
            try:
                task["action"]["notification"]["httpCustom"]["qs"]
            except KeyError:
                pass
            else:
                task["action"]["notification"]["httpCustom"]["params"] = task["action"]["notification"]["httpCustom"].pop("qs")
            return task
        else:
            return {"error":"Task not found"}

    def update_task(self, _id, attrs):
        task = self.get_task(_id)
        task.update(attrs)
        task["subject"] = task.pop("trigger")
        task["notification"] = task.pop("action")
        
        if "condition" in task["subject"]:
            task["subject"]["condition"] = { "q": task["subject"]["condition"] }

        try:
            task["action"]["notification"]["httpCustom"]["params"]
        except KeyError:
            pass
        else:
            task["action"]["notification"]["httpCustom"]["qs"] = task["action"]["notification"]["httpCustom"].pop("params")
        task.pop('id')
        headers = {
            'content-type': 'application/json'
            #'content-length': str(len(json.dumps(attrs).encode('utf-8')))
            }
        headers.update(self.iota['headers'])
        r = requests.request("PATCH", url=self.orion['url'] + "/v2/subscriptions/" + _id, data=attrs, headers=headers)

        if r.status_code == 204:
            return {"status":"updated","response":r.json()}
        else:
            return {"status":"error","response":r.json()}

    def delete_task(self, _id):
        headers = self.iota['headers']
        r = requests.request(
            'DELETE', url=self.orion['url'] + '/v2/subscriptions/' + _id, headers=headers)
        if r.status_code == 204:
            log("delete_task()", r.text + ' deleted', 'info')
            return r.json(), 204
        else:
            log("delete_task()", "An error occured -- " + r.text)
            return r.json(), 400

    def create_historical_device(self, historical):
        has_historical = False
        for conn in self.app_client.dsc:
            if conn['type'] != 'cloudant':
                continue
            for rule in conn.rules:
                try:
                    if rule['type'] == 'event' and rule['selector']['deviceType'] == '*' and rule['selector']['eventId'] == '*':
                        has_historical = True
                        break
                except KeyError as ke:
                    log(TAG, ke)
        if has_historical:
            log(TAG, "Historical já existente")
        else:
            log(TAG, "Historical a ser criado")
            # Configure the binding
            historical = {
                "name": "mycloudant",
                "type": "cloudant",
                "description": "uma descrição",
                "timezone": 'America/Sao_Paulo',
                "credentials": {
                    "host": "https://6c505913-4696-4168-b63c-4354e4b13730-bluemix:ec07709b77140b43000189bf4c2ba78db446798860c55bfa80f19b7c2cc84aa3@6c505913-4696-4168-b63c-4354e4b13730-bluemix.cloudantnosqldb.appdomain.cloud",
                    "port": 443,
                    "username": "6c505913-4696-4168-b63c-4354e4b13730-bluemix",
                    "password": "ec07709b77140b43000189bf4c2ba78db446798860c55bfa80f19b7c2cc84aa3"
                }
            }

            service = self.app_client.serviceBindings.create(historical)
            # Set up the connector
            connector = self.app_client.dsc.create(name="connector1", type="cloudant", serviceId=service.id,
                                                   description="Uma descrição", timezone=historical['timezone'], enabled=True)
            # Set up destinations
            connector.destinations.create(
                name="events", bucketInterval="MONTH")
            #connector.destinations.create(name="state", bucketInterval="MONTH")
            # Set up rules
            rule = connector.rules.createEventRule(
                name="allevents", destinationName="events", typeId="*", eventId="*", description="Uma regra", enabled=True)
            #rule2 = connector.rules.createStateRule(name="allstate", destinationName="state", logicalInterfaceId="*")
            log(TAG, "Historical criado")

    def create_historical_entity(self, historical):
        self.historical_entity = historical['activate']
    
    @staticmethod
    def get_schema():
        return {
  "title": "Fiware",
  "type": "object",
  "required": [],
  "additionalProperties": False,
  "properties": {
    "iota": {
      "title": "IoT Agent",
      "type": "object",
      "additionalProperties": False,
      "required": [
        "headers",
        "url"
      ],
      "properties": {
        "url": {
          "type": "string",
          "format": "url"
        },
        "headers": {
          "title": "HTTP Headers",
          "type": "object",
          "required": [
            "fiware-service",
            "fiware-servicepath"
          ],
          "properties": {
            "fiware-service": {
              "type": "string"
            },
            "fiware-servicepath": {
              "type": "string"
            }
          }
        }
      }
    },
    "orion": {
      "title": "Orion Broker",
      "additionalProperties": False,
      "type": "object",
      "required": [
        "url"
      ],
      "properties": {
        "url": {
          "type": "string",
          "format": "url"
        }
      }
    },
    "cygnus": {
      "title": "Cygnus Historical",
      "type": "object",
      "required": [
        "url",
        "notify_root"
      ],
      "additionalProperties": False,
      "properties": {
        "url": {
          "type": "string",
          "format": "url"
        },
        "notify_root": {
          "title": "Notify Root",
          "type": "string"
        }
      }
    },
    "mongodb": {
      "title": "Mongo DB",
      "type": "object",
      "url": {
        "type": "string",
        "format": "url"
      }
    },
    "historical": {
      "title": "Historical Storage?",
      "type": "object",
      "required": [
        "activate"
      ],
      "additionalProperties": False,
      "properties": {
        "activate": {
          "type": "boolean"
        }
      }
    }
  }
}