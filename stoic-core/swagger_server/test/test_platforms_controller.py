# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.body import Body  # noqa: E501
from swagger_server.models.body1 import Body1  # noqa: E501
from swagger_server.models.inline_response200 import InlineResponse200  # noqa: E501
from swagger_server.test import BaseTestCase


class TestPlatformsController(BaseTestCase):
    """PlatformsController integration test stubs"""

    def test_create_broker(self):
        """Test case for create_broker

        Create a new broker
        """
        body = Body1()
        response = self.client.open(
            '/broker',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_delete_broker(self):
        """Test case for delete_broker

        Deletes a broker
        """
        response = self.client.open(
            '/broker/{brokerId}'.format(brokerId='brokerId_example'),
            method='DELETE')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_broker_by_id(self):
        """Test case for get_broker_by_id

        Find broker by ID
        """
        response = self.client.open(
            '/broker/{brokerId}'.format(brokerId='brokerId_example'),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_list_brokers(self):
        """Test case for list_brokers

        List all the brokers
        """
        response = self.client.open(
            '/broker',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_update_broker(self):
        """Test case for update_broker

        Update an existing broker
        """
        body = Body()
        response = self.client.open(
            '/broker',
            method='PUT',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
