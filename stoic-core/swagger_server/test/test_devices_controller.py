# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.body1 import Body1  # noqa: E501
from swagger_server.models.body2 import Body2  # noqa: E501
from swagger_server.models.body3 import Body3  # noqa: E501
from swagger_server.test import BaseTestCase


class TestDevicesController(BaseTestCase):
    """DevicesController integration test stubs"""

    def test_create_device(self):
        """Test case for create_device

        Create a new device
        """
        body = Body3()
        response = self.client.open(
            '/device',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_delete_device(self):
        """Test case for delete_device

        Deletes a device
        """
        response = self.client.open(
            '/device/{deviceId}'.format(deviceId='deviceId_example'),
            method='DELETE')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_device_by_id(self):
        """Test case for get_device_by_id

        Find device by ID
        """
        response = self.client.open(
            '/device/{deviceId}'.format(deviceId='deviceId_example'),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_list_devices(self):
        """Test case for list_devices

        List all the devices
        """
        response = self.client.open(
            '/device',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_update_device(self):
        """Test case for update_device

        Update an existing device
        """
        body = Body2()
        response = self.client.open(
            '/device',
            method='PUT',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
