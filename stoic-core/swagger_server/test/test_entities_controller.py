# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.body3 import Body3  # noqa: E501
from swagger_server.models.body4 import Body4  # noqa: E501
from swagger_server.models.body5 import Body5  # noqa: E501
from swagger_server.test import BaseTestCase


class TestEntitiesController(BaseTestCase):
    """EntitiesController integration test stubs"""

    def test_create_entity(self):
        """Test case for create_entity

        Create a new entity
        """
        body = Body5()
        response = self.client.open(
            '/entity',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_delete_entity(self):
        """Test case for delete_entity

        Deletes a entity
        """
        response = self.client.open(
            '/entity/{entityId}'.format(entityId='entityId_example'),
            method='DELETE')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_entity_by_id(self):
        """Test case for get_entity_by_id

        Find entity by ID
        """
        response = self.client.open(
            '/entity/{entityId}'.format(entityId='entityId_example'),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_list_entities(self):
        """Test case for list_entities

        List all the entities
        """
        response = self.client.open(
            '/entity',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_update_entity(self):
        """Test case for update_entity

        Update an existing entity
        """
        body = Body4()
        response = self.client.open(
            '/entity',
            method='PUT',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
