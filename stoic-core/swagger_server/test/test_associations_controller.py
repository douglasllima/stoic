# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.body6 import Body6  # noqa: E501
from swagger_server.models.body7 import Body7  # noqa: E501
from swagger_server.models.inline_response2001 import InlineResponse2001  # noqa: E501
from swagger_server.test import BaseTestCase


class TestAssociationsController(BaseTestCase):
    """AssociationsController integration test stubs"""

    def test_create_association(self):
        """Test case for create_association

        Create a new association
        """
        body = Body7()
        response = self.client.open(
            '/association',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_delete_association(self):
        """Test case for delete_association

        Deletes a association
        """
        response = self.client.open(
            '/association/{associationId}'.format(associationId='associationId_example'),
            method='DELETE')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_association_by_id(self):
        """Test case for get_association_by_id

        Find association by ID
        """
        response = self.client.open(
            '/association/{associationId}'.format(associationId='associationId_example'),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_list_associations(self):
        """Test case for list_associations

        List all the associations
        """
        response = self.client.open(
            '/association',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_update_association(self):
        """Test case for update_association

        Update an existing association
        """
        body = Body6()
        response = self.client.open(
            '/association',
            method='PUT',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
