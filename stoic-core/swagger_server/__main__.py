#!/usr/bin/env python3

import connexion

from swagger_server import encoder, utils

def main():
    app = connexion.App(__name__, specification_dir='./swagger/')
    app.app.json_encoder = encoder.JSONEncoder
    app.add_api('swagger.yaml', arguments={'title': 'Stoic API'})
    utils.get_brokers_connections(True, app.app)

    # @app.app.teardown_appcontext
    # def shutdown_session(exception=None):
    #     app.app.config['brokers'] = {}

    app.run(port=8080)


if __name__ == '__main__':
    main()
