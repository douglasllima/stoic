import ply.lex as lex
import re

reserved = {
#    'q' : 'QUERY',
#    'mq' : 'METADATA_QUERY'
}

tokens = [
    'FLOAT',
    'NUMBER',
    'STRING',
    'RANGE',
    'EQUAL',
    'SEQUAL',
    'UNEQUAL',
    'GREATER_THAN',
    'LESS_THAN',
    'GREATER_THAN_OR_EQUAL',
    'LESS_THAN_OR_EQUAL',
    'MATCH',
    'NOT',
    'DOT',
    #'L_SQUOTE',
    #'R_SQUOTE',
    'COMMA',
    'SEMICOLON',
    'ID',
    'REGEX'
]

tokens = list(reserved.values()) + tokens

def t_FLOAT(t):
    #r'\d+[e|E|\.]\d+'
    r'(-?\d+e-?\d+)|(-?\d+\.\d+)'
    try:
        t.value = str(float(t.value))
    except ValueError:
        print("Float value too large %f", t.value)
        t.value = 0
    return t

def t_NUMBER(t):
    r'\d+'
    try:
        t.value = str(int(t.value))
    except ValueError:
        print("Integer value too large %d", t.value)
        t.value = 0
    return t

t_STRING = r'\'[^\']*\''
t_RANGE = r'\.\.'
t_EQUAL = r'(==|:)'
t_SEQUAL = r'='
t_UNEQUAL = r'!='
t_GREATER_THAN = r'>'
t_LESS_THAN = r'<'
t_GREATER_THAN_OR_EQUAL = r'>='
t_LESS_THAN_OR_EQUAL = r'<='
t_MATCH = r'~='
t_NOT = r'!'
t_DOT = r'\.'
#t_L_SQUOTE = r'\''
#t_R_SQUOTE = r'\''
t_COMMA = r','
t_SEMICOLON = r';'

def t_REGEX(t):
    r'((?<=~=)(.*)(?=(;))|(?<=~=)(.*)(?=($)))'
    t.type = 'REGEX'
    return t

def t_ID(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value,'ID')    # Check for reserved words
    return t

# Define a rule so we can track line numbers
def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

# A string containing ignored characters (spaces and tabs)
t_ignore  = ' \t'

 # Error handling rule
def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)


lexer = lex.lex()

fmt = None
ids = {}

def p_program (t):
    "program : expression"
    #'''program : QUERY SEQUAL expression
    #            | METADATA_QUERY SEQUAL expression'''
    '''
    if t[1] == 'q':
        fmt = 'query'
    elif t[1] == 'mq':
        fmt = 'metadata'
    print(t[3])'''
    print(t[1])

def p_expression_and (t):
    "expression : expression SEMICOLON expression"
    t[0] = '(' + t[1] + ') and (' + t[3] + ')'

def p_expression_op (t):
    '''expression :   path EQUAL term0
                  |   path UNEQUAL term0
                  |   path GREATER_THAN term1
                  |   path LESS_THAN term1
                  |   path GREATER_THAN_OR_EQUAL term1
                  |   path LESS_THAN_OR_EQUAL term1
                  |   path MATCH REGEX'''
    if t[2] == '==' or t[2] == ':':
        if 'in' in t[3]:
            t[0] = '$CODE$' + t[1] + ' ' + t[3]
        else:
            t[0] = '$CODE$' + t[1] + '==' + t[3]
    elif t[2] == '!=':
        if 'in' in t[3]:
            t[0] = '$CODE$' + t[1] + ' not ' + t[3]
        else:
            t[0] = '$CODE$' + t[1] + '!=' + t[3]
    elif t[2] == '>=': t[0] = '$CODE$' + t[1] + '>=' + t[3]
    elif t[2] == '<=': t[0] = '$CODE$' + t[1] + '<=' + t[3]
    elif t[2] == '>': t[0] = '$CODE$' + t[1] + '>' + t[3]
    elif t[2] == '<': t[0] = '$CODE$' + t[1] + '<' + t[3]
    elif t[2] == '~=': #t[0] = t[1] + '~=' + t[3]
        t[0] = "bool(re.search('"+t[3]+"',$CODE$"+t[1]+"))"

def p_path (t):
    '''path   : path DOT path'''
    t[0] = t[1] + t[3]

def p_path_id_string(t):
    '''path : ID
            | STRING'''
    if t[1][0] == '\'':
        t[0] = '[' + t[1] + ']'
    else:
        t[0] = '[\'' + t[1] + '\']'

def p_expression_not_id (t):
    "expression : NOT ID"
    t[0] = "'" + t[2] + "' not in $CODE$" 

def p_expression_id(t):
    '''expression : ID'''
    t[0] = "'" + t[1] + "' in $CODE$" 

def p_term0_range (t):
    "term0     :   term1 RANGE term1"
    t[0] = 'in range(' + t[1] + ',' + t[3] + ')'

def p_term0 (t):
    "term0     :   term2"
    t[0] = 'in [' + t[1] + ']'

def p_term1 (t):
    '''term1     :   NUMBER
                 |   FLOAT'''
    t[0] = t[1]

'''term2  :   term2 COMMA term2
          |   STRING
          |   term1'''
def p_term2_comma (t):
    "term2     :   term2 COMMA term2"
    t[0] = t[1] + ',' + t[3]

def p_term2 (t):
    '''term2 : STRING
            | ID
            | term1'''
    if re.match('(-?\d+e-?\d+)|(-?\d+(\.\d+)?)', t[1]):
        t[0] = t[1]
    elif t[1][0] != '\'':
        t[0] = '\'' + t[1] + '\''
    else:
        t[0] = t[1]

def p_error(t):
    print("Syntax error at '%s'" % t.value)

import ply.yacc as yacc
parser = yacc.yacc(debug=True)
'''
while True:
    data = str(input("teste>"))
    lexer.input(data)

    # Tokenize
    while True:
        tok = lexer.token()
        if not tok:
    	    break      # No more input
        print(tok)
'''

import io
from contextlib import redirect_stdout

def parseQuery(query, code = None):
    f = io.StringIO()
    with redirect_stdout(f):
        parser.parse(query)
    exp = f.getvalue()
    print("parse ==>> " + exp)
    if code:
        return exp.replace("$CODE$", code)
    return exp

'''
while True:
    try:
        s=input('query>')
    except EOFError:
        break
    parser.parse(s)
'''
