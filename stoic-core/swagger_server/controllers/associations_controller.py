import connexion
import six

from swagger_server.models.body6 import Body6  # noqa: E501
from swagger_server.models.body7 import Body7  # noqa: E501
from swagger_server.models.inline_response2001 import InlineResponse2001  # noqa: E501
from swagger_server import util

from swagger_server.services import associations_service

def create_association(body):  # noqa: E501
    """Create a new association

     # noqa: E501

    :param body: association description to be inserted in platform
    :type body: dict | bytes

    :rtype: None
    """
    broker = connexion.request.headers['broker']
    if connexion.request.is_json:
        body = Body7.from_dict(connexion.request.get_json())  # noqa: E501
    return associations_service.create_association(broker, connexion.request.get_json())


def delete_association(associationId):  # noqa: E501
    """Deletes a association

     # noqa: E501

    :param associationId: association id to delete
    :type associationId: str

    :rtype: None
    """
    broker = connexion.request.headers['broker']
    return associations_service.delete_association(broker, associationId)


def get_association_by_id(associationId):  # noqa: E501
    """Find association by ID

    Returns a single association # noqa: E501

    :param associationId: ID of association to return
    :type associationId: str

    :rtype: Body7
    """
    broker = connexion.request.headers['broker']
    return associations_service.get_association_by_id(broker, associationId)


def list_associations():  # noqa: E501
    """List all the associations

    List all the associations # noqa: E501


    :rtype: List[InlineResponse2001]
    """
    broker = connexion.request.headers['broker']

    return associations_service.list_associations(
        broker, 
        entity_type = connexion.request.args.get('entity_type'),
        entity_id = connexion.request.args.get('entity_id'),
        device_type = connexion.request.args.get('entity_type'),
        device_id = connexion.request.args.get('entity_id')
    )



def update_association(body):  # noqa: E501
    """Update an existing association

     # noqa: E501

    :param body: association description that needs to be added to the store
    :type body: dict | bytes

    :rtype: None
    """
    broker = connexion.request.headers['broker']
    return associations_service.update_association(broker, connexion.request.get_json())
