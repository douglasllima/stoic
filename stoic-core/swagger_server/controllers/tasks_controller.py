import connexion
import six

from swagger_server.models.body8 import Body8  # noqa: E501
from swagger_server.models.body9 import Body9  # noqa: E501
from swagger_server.models.inline_response2002 import InlineResponse2002  # noqa: E501
from swagger_server.models.inline_response2003 import InlineResponse2003  # noqa: E501
from swagger_server import util

from swagger_server.services import tasks_service

def create_task(body):  # noqa: E501
    """Create a new task

     # noqa: E501

    :param body: task description to be inserted in platform
    :type body: dict | bytes

    :rtype: None
    """
    broker = connexion.request.headers['broker']

    if connexion.request.is_json:
        body = Body9.from_dict(connexion.request.get_json())  # noqa: E501
    return tasks_service.create_task(broker, connexion.request.get_json())


def delete_task(taskId):  # noqa: E501
    """Deletes a task

     # noqa: E501

    :param taskId: task id to delete
    :type taskId: str

    :rtype: None
    """
    broker = connexion.request.headers['broker']
    return tasks_service.delete_task(broker, taskId)


def get_task_by_id(taskId):  # noqa: E501
    """Find task by ID

    Returns a single task # noqa: E501

    :param taskId: ID of task to return
    :type taskId: str

    :rtype: InlineResponse2003
    """
    broker = connexion.request.headers['broker']
    return tasks_service.get_task_by_id(broker, taskId)


def list_tasks():  # noqa: E501
    """List all the tasks

    List all the tasks # noqa: E501


    :rtype: List[InlineResponse2002]
    """
    broker = connexion.request.headers.get("broker")
    entity_type = connexion.request.args.get('entity_type')
    entity_id = connexion.request.args.get('entity_id')
    return tasks_service.list_tasks(broker,entity_type,entity_id)


def update_task(body,taskId):  # noqa: E501
    """Update an existing task

     # noqa: E501

    :param body: task description that needs to be added to the store
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = Body8.from_dict(connexion.request.get_json())  # noqa: E501
    broker = connexion.request.headers['broker']
    return tasks_service.update_task(broker, taskId, connexion.request.get_json())
