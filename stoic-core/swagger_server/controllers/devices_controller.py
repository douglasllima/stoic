import connexion
import six

from swagger_server.models.body1 import Body1  # noqa: E501
from swagger_server.models.body2 import Body2  # noqa: E501
from swagger_server.models.body3 import Body3  # noqa: E501
from swagger_server import util

from swagger_server.services import platforms_service
from swagger_server.services import devices_service

from swagger_server.middlewares import *

def create_device(body):  # noqa: E501
    """Create a new device

     # noqa: E501

    :param body: device description to be inserted in platform
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = Body3.from_dict(connexion.request.get_json())  # noqa: E501
        broker = connexion.request.headers['broker']
    return devices_service.create_device(broker, connexion.request.get_json())

def delete_device(deviceId):  # noqa: E501
    """Deletes a device

     # noqa: E501

    :param deviceId: device id to delete
    :type deviceId: str

    :rtype: None
    """
    broker = connexion.request.headers['broker']
    _type = deviceId.split('_')[0]
    _id = deviceId.split('_',1)[1]
    return devices_service.delete_device(broker, _type, _id)

def get_device_by_id(deviceId):  # noqa: E501
    """Find device by ID

    Returns a single device # noqa: E501

    :param deviceId: ID of device to return
    :type deviceId: str

    :rtype: Body3
    """
    broker = connexion.request.headers['broker']
    _type = deviceId.split('_')[0]
    _id = deviceId.split('_',1)[1]
    return devices_service.get_device_by_id(broker, _type, _id)

def list_devices():  # noqa: E501
    """List all the devices
    List all the devices # noqa: E501
    :rtype: Body1
    """
    try:
        broker = connexion.request.headers['broker']
    except:
        broker = None
    return devices_service.list_devices(broker)


def update_device(body):  # noqa: E501
    """Update an existing device

     # noqa: E501

    :param body: device description that needs to be added to the store
    :type body: dict | bytes

    :rtype: None
    """
    broker = connexion.request.headers['broker']
    if connexion.request.is_json:
        # body = Body2.from_dict(connexion.request.get_json())  # noqa: E501
        return devices_service.update_device(broker, connexion.request.get_json())

def update_brokers(invalidate = False):
    global brokers
    if invalidate:
        brokers = {}
        p = platforms_service.list_brokers()
        for broker in p:
            try:
                _type = broker['type']
                if _type == 'fiware':
                    mw = fiware_middleware
                elif _type == 'watson':
                    mw = watson_middleware
                brokers[broker['id']] = mw(broker, None)
            except Exception as e:
                print(e)        
    try:
        brokers
    except:
        return update_brokers(True)
    return brokers
    # global brokers
    # brokers = {}
    # p = json.loads(get_active_text(request))
    # for broker, content in p['brokers'].items():
    #     _type = content['type']
    #     if _type == 'fiware':
    #         mw = fiware_middleware
    #     elif _type == 'watson':
    #         mw = watson_middleware
    #     brokers[broker] = mw(content, None)
    # return brokers