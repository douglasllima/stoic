import connexion
import six

from swagger_server.models.body import Body  # noqa: E501
from swagger_server.models.body1 import Body1  # noqa: E501
from swagger_server.models.inline_response200 import InlineResponse200  # noqa: E501
from   swagger_server.services import platforms_service
from swagger_server import util

from pymongo import MongoClient
from bson.objectid import ObjectId

def create_broker(body):  # noqa: E501
    """Create a new broker

     # noqa: E501

    :param body: Broker description to be inserted in platform
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        return { '_id': platforms_service.create_broker(body) }
    else:
        return { 'error': 'Document not inserted' }

    # client = MongoClient('localhost', 27017)
    # db_stoicws = client['stoic_ws']
    # brokers = db_stoicws['brokers']
    
    # if connexion.request.is_json:
    #     body = Body1.from_dict(connexion.request.get_json())  # noqa: E501
    #     broker = body.to_dict()
    #     broker['_id'] = broker['id']
    #     broker.pop('id')
    #     return {
    #         '_id': str(brokers.insert_one(broker).inserted_id)
    #         }
    # return 'Error'


def delete_broker(brokerId):  # noqa: E501
    """Deletes a broker

     # noqa: E501

    :param brokerId: Broker id to delete
    :type brokerId: str

    :rtype: None
    """
    return { 'deleted': platforms_service.delete_broker(brokerId) }

    # client = MongoClient('localhost', 27017)
    # db_stoicws = client['stoic_ws']
    # brokers = db_stoicws['brokers']
    # return {'deleted': brokers.delete_one({'_id': ObjectId(brokerId)})}


def get_broker_by_id(brokerId):  # noqa: E501
    """Find broker by ID

    Returns a single broker # noqa: E501

    :param brokerId: ID of broker to return
    :type brokerId: str

    :rtype: Body1
    """
    return platforms_service.get_broker_by_id(brokerId)

    # client = MongoClient('localhost', 27017)
    # db_stoicws = client['stoic_ws']
    # brokers = db_stoicws['brokers']
    # return brokers.find({'_id': ObjectId(brokerId)})


def list_brokers():  # noqa: E501
    """List all the brokers

    List all the brokers # noqa: E501


    :rtype: InlineResponse200
    """
    return platforms_service.list_brokers()
    # client = MongoClient('localhost', 27017)
    # db_stoicws = client['stoic_ws']
    # brokers = db_stoicws['brokers']
    # cursor = brokers.find({})
    # return list(cursor)

def update_broker(body):  # noqa: E501
    """Update an existing broker

     # noqa: E501

    :param body: Broker description that needs to be added to the store
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = connexion.request.get_json()  # noqa: E501
        return { 'updated': platforms_service.update_broker(body) }
    return { 'error': 'Document not updated' }
    # if connexion.request.is_json:
    #     body = Body.from_dict(connexion.request.get_json())  # noqa: E501
    # return 'do some magic!'

def get_schema(brokerType):
    return platforms_service.get_schema_by_type(brokerType)