import connexion
import six

from swagger_server.models.body3 import Body3  # noqa: E501
from swagger_server.models.body4 import Body4  # noqa: E501
from swagger_server.models.body5 import Body5  # noqa: E501
from swagger_server import util

from swagger_server.services import entities_service

import requests as req

def create_entity(body):  # noqa: E501
    """Create a new entity

     # noqa: E501

    :param body: entity description to be inserted in platform
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        #body = Body3.from_dict(connexion.request.get_json())  # noqa: E501
        body = connexion.request.get_json()
        brokers = connexion.request.headers['brokers'].split(',')
        #r = req.get("http://localhost:3000/clusters", params=dict(group = brokers, entity_id = body['id'], entity_type = body['type']))

        return entities_service.create_entity(brokers, connexion.request.get_json())


def delete_entity(entityId, type):  # noqa: E501
    """Deletes a entity

     # noqa: E501

    :param entityId: entity id to delete
    :type entityId: str

    :rtype: None
    """
    brokers = connexion.request.headers['brokers'].split(',')
    # _type = entityId.split('_')[0]
    # _id = entityId.split('_',1)[1]
    _id = entityId
    _type = type
    return entities_service.delete_entity(brokers, _type, _id)


def get_entity_by_id(entityId):  # noqa: E501
    """Find entity by ID

    Returns a single entity # noqa: E501

    :param entityId: ID of entity to return
    :type entityId: str

    :rtype: Body5
    """
    broker = connexion.request.headers['broker']
    _type = entityId.split('_')[0]
    _id = entityId.split('_',1)[1]
    return entities_service.get_entity_by_id(broker, _type, _id)


def list_entities(clustered = False):  # noqa: E501
    """List all the entities

    List all the entities # noqa: E501


    :rtype: List[Body3]
    """
    
    return entities_service.list_entities(clustered), 200, {'Access-Control-Allow-Origin': '*'}


def update_entity(body):  # noqa: E501
    """Update an existing entity

     # noqa: E501

    :param body: entity description that needs to be added to the store
    :type body: dict | bytes

    :rtype: None
    """
    broker = connexion.request.headers['broker']
    if connexion.request.is_json:
        # body = Body2.from_dict(connexion.request.get_json())  # noqa: E501
        return entities_service.update_entity(broker, connexion.request.get_json())
