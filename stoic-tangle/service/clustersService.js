const MongoClient = require('mongodb').MongoClient;
const request = require('request');

module.exports = class ClustersService {

    constructor(mongo_url, db, collection) {
        this.mongo_url = mongo_url;
        this.db = db
        this.collection = collection
    }

    async intersection(set1, ...sets) {
        if(!sets.length) {
            return set1;
        }
        const tmp = [...set1].filter(x => sets.every(y => Array.isArray(y) ? y.includes(x) : y.has(x)));
        return Array.isArray(set1) ? tmp : new Set(tmp);
    }

    async listClusters(entity_type, entity_id) {
        let client = new MongoClient(this.mongo_url);

        try {
            await client.connect();
            console.log("Connected correctly to server");

            const db = client.db(this.db);

            // Get the collection
            const col = db.collection(this.collection);

            let $query = {
                "entity_id": entity_id,
                "entity_type": entity_type
            }
            console.log(JSON.stringify($query))
            let groups_docs = await col.find($query).toArray();
            let big_groups = []

            groups_docs.forEach((group_doc) => {
                big_groups.push(group_doc.group)
            })
            for (let i = 0; i < big_groups.length; i++) {
                for (let j = 0; j < big_groups.length; j++) {
                    if (i != j) {
                        let intersec = await this.intersection(big_groups[i], big_groups[j])
                        if (intersec.size > 0) {
                            big_groups[i].concat(big_groups[j])
                            big_groups[j].concat(big_groups[i])
                            big_groups[i] = Array.from(new Set(big_groups[i]))
                            big_groups[j] = Array.from(new Set(big_groups[j]))
                        }
                    }
                }
            }
            big_groups = big_groups.map(JSON.stringify)
            big_groups = Array.from(new Set(big_groups))
            big_groups = big_groups.map(JSON.parse)

            await client.close();
            return big_groups
        } catch (err) {
            console.log(err.stack);
            await client.close();
            return { "error": err.stack }
        }
    }

    async getCluster(entity_type, entity_id, group) {
        let client = new MongoClient(this.mongo_url);

        try {
            await client.connect();
            console.log("Connected correctly to server");

            const db = client.db(this.db);

            // Get the collection
            const col = db.collection(this.collection);
            let or = []
            if (group.length == 0) {
                client.close();
                return { "error": "group should be greather than 0" }
            }
            group.forEach(g => {
                or.push({ "group": { $all: [g] } })
            })
            let $query = {
                $or: or,
                "entity_id": entity_id,
                "entity_type": entity_type
            }
            console.log(JSON.stringify($query))
            let groups_docs = await col.find($query).toArray();
            let big_group = []
            groups_docs.forEach((group_doc) => {
                big_group = big_group.concat(group_doc.group)
            })
            big_group = Array.from(new Set(big_group))
            await client.close();
            return big_group
        } catch (err) {
            console.log(err.stack);
            await client.close();
            return { "error": err.stack }
        }
    }

    async createOrUpdateCluster(entity_type, entity_id, group) {
        let client = new MongoClient(this.mongo_url);

        try {
            await client.connect();
            console.log("Connected correctly to server");

            const db = client.db(this.db);

            // if (group instanceof Array) {
            //     let groups = []
            //     group.forEach(g=>{
            //         groups.push({
            //             "entity_type": entity_type,
            //             "entity_id": entity_id,
            //             "group": g
            //         })
            //     })
            //     // Insert multiple documents
            //     let r = db.collection('tangles').insertMany(groups);
            // } else if (group instanceof Object) {
            //     // Insert a single document
            //     let r = db.collection('tangles').insertOne(group);
            // }
            // Insert a single document
            let r = await db.collection('tangles').insertOne(
                {
                    "entity_id": entity_id,
                    "entity_type": entity_type,
                    "group": group
                }
            );
            let tasks = {}
            let new_task = {}
            for (let i = 0; i < group.length; i++) {
                tasks[group[i]] = await this.findTasks(entity_type, entity_id, group[i])
            }
            for (const [broker, task] of Object.entries(tasks)) {
                new_task[broker] = task[broker].filter(t => {
                    if ("condition" in t.trigger) {
                        if (t.trigger.condition == 'tangle') {
                            return true
                        }
                    }
                    return false
                })
              }
            for (const [broker, tasks] of Object.entries(new_task)) {
                if (tasks.length == 0) {
                    this.createTask(entity_type, entity_id, broker)
                }
            }
            await client.close();
            return { "inserted": r.insertedCount }

        } catch (err) {
            console.log(err.stack);
            client.close();
            return { "error": err.stack }
        }

        // Close connection
        client.close();
    }

    async deleteCluster(entity_type, entity_id, group) {
        let client = new MongoClient(this.mongo_url);

        try {
            await client.connect();
            console.log("Connected correctly to server");

            const db = client.db(this.db);

            // Get the collection
            const col = db.collection(this.collection);
            let or = []
            if (group.length == 0) {
                client.close();
                return { "error": "group should be greather than 0" }
            }
            group.forEach(group => {
                or.push({ "group": { $all: [group] } })
            })
            let $query = {
                $or: or,
                "entity_id": entity_id,
                "entity_type": entity_type
            }
            let groups_docs = await col.find($query).toArray();
            //groups_docs.forEach(group_doc=>{
            for (var j = 0; j < groups_docs.length; j++) {
                let group_doc = groups_docs[j]
                let $query = { _id: group_doc._id }
                let new_group = group_doc.group
                group.forEach(g=>{
                    let index = new_group.indexOf(g);
                    if (index > -1) {
                      new_group.splice(index, 1);
                    }
                })
                
                let tasks = []

                for (let i = 0; i < group.length; i++) {
                    tasks[group[i]] = await this.findTasks(entity_type, entity_id, group[i])
                    this.deleteTask(group[i], tasks[group[i]].id)
                }

                if (new_group.length > 0) {
                    col.updateOne(
                        $query,
                        {
                        $set: { "group": new_group },
                        $currentDate: { lastModified: true }
                        }
                    )
                } else {
                    col.deleteOne($query)
                }
            }
            //})

            await client.close();
            return {"deleted": true}
        } catch (err) {
            console.log(err.stack);
            client.close();
            return { "error": err.stack }
        }
    }

    async findTasks(entity_type, entity_id, broker) {
        function doRequest() {
            return new Promise(function (resolve, reject) {
                var options = {
                    'method': 'GET',
                    'url': `http://localhost:8080/task?entity_type=${entity_type}&entity_id=${entity_id}`,
                    'headers': {
                      'broker': broker
                    }
                  };
                request(options, function (error, response) { 
                    if (error) throw new Error(error);
                    console.log(response.body);
                    resolve(JSON.parse(response.body))
                });
            });
          }
        let tasks = await doRequest()
        return tasks;
    }

    async createTask(entity_type, entity_id, broker) {
        function doRequest() {
            return new Promise(function (resolve, reject) {
                let task = {
                    "action": {
                      "http": {
                        "url": "http://localhost:3000/notify"
                      }
                    },
                    "description":`Tangle reserved: [${broker}/${entity_type}/${entity_id}]`,
                    "trigger": {
                      "condition": "tangle",
                      "entities": [
                        {
                          "id": entity_id,
                          "type": entity_type
                        }
                      ]
                    }
                  }
                var options = {
                    'method': 'POST',
                    'url': 'http://localhost:8080/task',
                    'headers': {
                        'broker': 'orion_fiware',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(task)
                };
                request(options, function (error, response) { 
                    if (error) throw new Error(error);
                    console.log(response.body);
                });                  
            });
          }
        let resp = await doRequest()
        return resp;
    }

    async deleteTask(broker, task_id) {
        function doRequest() {
            return new Promise(function (resolve, reject) {
                var options = {
                    'method': 'DELETE',
                    'url': `localhost:8080/task/${task_id}`,
                    'headers': {
                        'broker': broker
                    }
                };
                request(options, function (error, response) { 
                    if (error) throw new Error(error);
                    console.log(response.body);
                });                  
            });
          }
        let resp = await doRequest()
        return resp;
    }

}