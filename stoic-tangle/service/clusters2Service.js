const MongoClient = require('mongodb').MongoClient;
const request = require('request');

module.exports = class ClustersService {

    constructor(mongo_url, db, collection) {
        this.mongo_url = mongo_url;
        this.db = db
        this.collection = collection
    }

    async intersection(set1, ...sets) {
        if(!sets.length) {
            return set1;
        }
        const tmp = [...set1].filter(x => sets.every(y => Array.isArray(y) ? y.includes(x) : y.has(x)));
        return Array.isArray(set1) ? tmp : new Set(tmp);
    }

    async createOrUpdateCluster(entity_type, entity_id, brokers) {
        let client = new MongoClient(this.mongo_url);

        try {
            await client.connect();
            console.log("Connected correctly to server MongoDB");

            const db = client.db(this.db);

            let contem = brokers
            let ids = []

            // Get the collection
            const col = db.collection(this.collection);

            let $query = {
                        $or: [],
                        "entity_id": entity_id,
                        "entity_type": entity_type
                    }
            $query.$or.push({ brokers: [] })
            for (let i = 0; i < brokers.length; i++) {
                $query.$or.push({ brokers: { $all: [brokers[i]]} })
            }
            console.log(JSON.stringify($query))
            let groups_docs = await col.find($query).toArray();
            ids.concat(groups_docs.map(group => {
                    return group._id
                })
            )
            groups_docs.forEach(group => {
                contem = contem.concat(group.brokers)
            })
            contem = Array.from(new Set(contem))
            await col.deleteMany($query)
            let r = null;
            if (contem.length > 0) {
                r = col.insertOne({
                    "entity_type": entity_type,
                    "entity_id": entity_id,
                    "brokers": contem
                })
            }

            let tasks = {}
            let new_task = {}
            for (let i = 0; i < contem.length; i++) {
                tasks[contem[i]] = await this.findTasks(entity_type, entity_id,contem[i])
            }
            for (const [broker, task] of Object.entries(tasks)) {
                new_task[broker] = task.filter(t => {
                    if ("condition" in t.trigger) {
                        //if (t.trigger.condition == "!tangle") {
                        if (t.trigger.condition == "") {
                            return true
                        }
                    }
                    return false
                })
              }
            for (const [broker, tasks] of Object.entries(new_task)) {
                if (tasks.length == 0) {
                    this.createTask(entity_type, entity_id, broker)
                }
            }
            await client.close();
            if (r != null)
                return { "inserted": r.insertedCount }
            else
                return { "inserted": 0 };

        } catch (err) {
            console.log(err.stack);
            await client.close();
            return { "error": err.stack }
        }

        // Close connection
        await client.close();
    }

    async listClusters() {
        let client = new MongoClient(this.mongo_url);

        try {
            await client.connect();
            console.log("Connected correctly to server MongoDB");

            const db = client.db(this.db);

            // Get the collection
            const col = db.collection(this.collection);

            let groups_docs = await col.find().toArray();

            await client.close();
            return groups_docs
        } catch (err) {
            console.log(err.stack);
            await client.close();
            return { error: err.stack }
        }
    }

    async listClustersById(entity_type, entity_id, brokers) {
        let client = new MongoClient(this.mongo_url);

        try {
            await client.connect();
            console.log("Connected correctly to MongoDB");

            const db = client.db(this.db);

            // Get the collection
            const col = db.collection(this.collection);

            let $query = {
                "brokers": { $all: brokers },
                "entity_id": entity_id,
                "entity_type": entity_type
            }
            console.log("Consultando: " + JSON.stringify($query))
            let groups_docs = await col.find($query).toArray();
            
            await client.close();
            return groups_docs
        } catch (err) {
            console.log(err.stack);
            await client.close();
            return { "error": err.stack }
        }
    }

    async deleteCluster(entity_type, entity_id, brokers) {
        let client = new MongoClient(this.mongo_url);

        try {
            await client.connect();
            console.log("Connected correctly to server MongoDB");

            const db = client.db(this.db);

            // Get the collection
            const col = db.collection(this.collection);

            let $query = {
                'entity_type': entity_type,
                'entity_id': entity_id,
                '$or': [
                    {
                        'brokers': {
                            $all: brokers
                        }
                    }
                ]
            }

            let groups_docs = await col.find($query).toArray()
            groups_docs.map(group_doc => {
                group_doc.brokers = group_doc.brokers.filter(
                    broker => {
                        return !(brokers.indexOf(broker) > -1)
                    }
                )
                if (group_doc.brokers.length > 0) {
                    col.updateOne(
                        {_id: group_doc._id},
                        {
                            $set: { "brokers": group_doc.brokers },
                            $currentDate: { lastModified: true }
                        })
                    }
                else {
                    col.deleteOne({_id: group_doc._id})
                }
            })

            // let or = []
            // if (group.length == 0) {
            //     client.close();
            //     return { "error": "group should be greather than 0" }
            // }
            // group.forEach(group => {
            //     or.push({ "group": { $all: [group] } })
            // })
            // let $query = {
            //     $or: or,
            //     "entity_id": entity_id,
            //     "entity_type": entity_type
            // }
            // groups_docs = await col.find($query).toArray();
            // //groups_docs.forEach(group_doc=>{
            for (let i = 0; i < brokers.length; i++) {
                let tasks = await this.findTasks(entity_type, entity_id, brokers[i])
                tasks.map(task => {
                    if ('condition' in task.trigger) {
                        if (task.trigger.condition == '') {
                            this.deleteTask(brokers[i], task.id)
                        }
                    }
                })
            }

            await client.close();
            return {"deleted": true}
        } catch (err) {
            console.log(err.stack);
            client.close();
            return { "error": err.stack }
        }
    }

    async findTasks(entity_type, entity_id, broker) {
        function doRequest() {
            return new Promise(function (resolve, reject) {
                var options = {
                    'method': 'GET',
                    'url': `http://localhost:8080/task?entity_type=${entity_type}&entity_id=${entity_id}`,
                    'headers': {
                      'broker': broker
                    }
                  };
                request(options, function (error, response) { 
                    if (error) throw new Error(error);
                    console.log(response.body);
                    resolve(JSON.parse(response.body))
                });
            });
          }
        let tasks = await doRequest()
        return tasks[broker];
    }

    async createTask(entity_type, entity_id, broker) {
        function doRequest() {
            return new Promise(function (resolve, reject) {
                let task = {
                    "action": {
                      "http": {
                        "url": `http://172.19.0.1:3000/notify/${broker}`
                      }
                    },
                    "description":`Tangle reserved: [${broker}/${entity_type}/${entity_id}]`,
                    "trigger": {
                      //"condition": "!tangle",
                      "condition": "",
                      "entities": [
                        {
                          "id": entity_id,
                          "type": entity_type
                        }
                      ]
                    }
                  }
                var options = {
                    'method': 'POST',
                    'url': 'http://localhost:8080/task',
                    'headers': {
                        'broker': broker,
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(task)
                };
                request(options, function (error, response) { 
                    if (error) throw new Error(error);
                    console.log(response.body);
                });                  
            });
          }
        let resp = await doRequest()
        return resp;
    }

    async deleteTask(broker, task_id) {
        function doRequest() {
            return new Promise(function (resolve, reject) {
                var options = {
                    'method': 'DELETE',
                    'url': `http://172.19.0.1:8080/task/${task_id}`,
                    'headers': {
                        'broker': broker
                    }
                };
                request(options, function (error, response) { 
                    if (error) throw new Error(error);
                    console.log(response.body);
                });                  
            });
          }
        let resp = await doRequest()
        return resp;
    }

}