var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');
//var request = require('sync-request');
const request = require('request');

var WatsonEntities = require('./watson/entitiesService')
var FiwareEntities = require('./fiware/entitiesService')
var ClusterService = require('./service/clusters2Service')
const cs = new ClusterService("mongodb://localhost:27017", "stoic-tangle", "tangles")

//console.log = () => {};

// Load the Cloudant library.
var Cloudant = require('@cloudant/cloudant');

var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

Object.defineProperty(String.prototype, 'hashCode', {
    value: function() {
      var hash = 0, i, chr;
      for (i = 0; i < this.length; i++) {
        chr   = this.charCodeAt(i);
        hash  = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
      }
      return hash;
    }
  });

var platforms = {}

// function getEnv() {
//     var env = {}
//     env = fs.readFileSync('environment.json', 'utf8')
//     return JSON.parse(env)
// }

function getEnv() {
    let res = request('GET', 'http://localhost:8080/broker');
    let brokers = JSON.parse(res.getBody('utf8'));
    return { "brokers": brokers };
}

function initializePlatforms() {
    var env = getEnv()

    env.brokers.forEach((broker) => {
        let GenericEntities = null;

        switch (broker.type) {
            case 'watson':
                GenericEntities = WatsonEntities;
                break;

            case 'fiware':
                GenericEntities = FiwareEntities;
                break;
        }

        platforms[broker.id] = new GenericEntities(broker);
    })
}

// app.post('/env', function(req, res) {
//     fs.writeFile(env_file, JSON.stringify(req.body), function (err) {
//       if (err) return console.log(err);
//       console.log('Updating enviroment');
//     });
// })
async function postEntity(entity, broker) {
    function doRequest() {
        return new Promise(function (resolve, reject) {
            var options = {
                'method': 'PUT',
                'url': `http://localhost:8080/entity`,
                'headers': {
                    'Content-Type': 'application/json',
                    'broker': broker
                },
                'body': JSON.stringify(entity)
              };
            request(options, function (error, response) { 
                if (error) {
                    console.log("Connection Refused")
                    return
                }
                console.log(response.body);
                resolve(response.body)
            });
        });
      }
    try {
        let resp = await doRequest()
        return resp
    } catch (e) {
        console.log(e)
        return {"error": e}
    }
}

let tangles = {}

function notify(req, res) {
    function order(unordered) {
        const ordered = {};
        Object.keys(unordered).sort().forEach(function (key) {
            ordered[key] = unordered[key];
        });
        return ordered;
    }

    if (!('broker_id' in req.query) && !('broker_id' in req.params)) {
        res.status(400).send({
            'error': 'Without broker_id',
            'name': 'BAD_REQUEST',
            'status': 400
        })
    }
    let broker_id = ('broker_id' in req.query) ? req.query.broker_id : req.params.broker_id
    console.log(JSON.stringify(req.body.data)); // the posted data

    var entities = req.body.data
    entities.forEach(entity => {
        //console.log(JSON.stringify(entity))
        let t_entity = {}
        let t_comp, t
        t_entity = order(entity)
        // delete t_entity.id
        // delete t_entity.type

        for (var [key, attr] of Object.entries(t_entity)) {
            if (attr.value)
                t_entity[key] = attr.value
        }

        t_comp = JSON.stringify(t_entity).hashCode()
        // if ("tangle" in entity) {
        //     t = entity.tangle.value
        //     if (t == t_comp) {
        //         console.log(`Colisão ${broker_id}:${entity.type}:${entity.id}`)
        //         return
        //     }
        // } else { console.log("Não tem tangle")}
        // entity.tangle = {
        //     value: t_comp,
        //     type: "Number"
        // }
        cs.listClustersById(entity.type, entity.id, [broker_id]).then(clusters => {
            for (var i = 0; i < clusters.length; i++) {
                let cluster = clusters[i]
                if (cluster.brokers.length == 1) {
                    return
                }
                let hash = `${t_entity.type}:${t_entity.id}:${JSON.stringify(cluster)}`

                function update(cluster) {
                    let brokers = cluster.brokers
                    brokers.splice(brokers.indexOf(broker_id),1)
                    console.log("Atualizado " + hash + " " + broker_id + " " + brokers)
                    brokers.forEach(broker => {
                        postEntity(entity, broker)
                    })
                }

                if (!(hash in tangles)) {
                    console.log("não há hash em tangles")
                    update(cluster)
                    tangles[hash] = t_comp
                } else {
                    console.log("há hash em tangles")
                    if (t_comp != tangles[hash]) {
                        console.log("hashs diferentes")
                        update(cluster)
                        tangles[hash] = t_comp
                    } else console.log("colidiu")
                }

                // console.log(`${t}==${t_comp}==${broker_id}`)
                // clusters.forEach(cluster => {
                //     cluster.brokers.forEach(broker => {
                //             postEntity(entity, broker)
                //     })
                // })
            }
        })
    })
    res.status(201).send({
        'message': 'Tangled'
    })
}

function notify2(req, res) {
    function order(unordered) {
        const ordered = {};
        Object.keys(unordered).sort().forEach(function (key) {
            ordered[key] = unordered[key];
        });
        return ordered;
    }

    if (!('broker_id' in req.query) && !('broker_id' in req.params)) {
        res.status(400).send({
            'error': 'Without broker_id',
            'name': 'BAD_REQUEST',
            'status': 400
        })
    }
    let broker_id = ('broker_id' in req.query) ? req.query.broker_id : req.params.broker_id
    console.log(req.body.data); // the posted data

    var entities = req.body.data
    entities.forEach(entity => {
        //console.log(JSON.stringify(entity))
        let t_entity = {}
        let t_comp, t
        t_entity = order(entity)
        delete t_entity.id
        delete t_entity.type
        delete t_entity.tangle
        for (var [key, attr] of Object.entries(t_entity)) {
            t_entity[key] = attr.value
        }

        t_comp = JSON.stringify(t_entity).hashCode()
        if ("tangle" in entity) {
            t = entity.tangle.value
            if (t == t_comp) {
                console.log(`Colisão ${broker_id}:${entity.type}:${entity.id}`)
                return
            }
        } else { console.log("Não tem tangle")}
        entity.tangle = {
            value: t_comp,
            type: "Number"
        }
        cs.listClustersById(entity.type, entity.id, [broker_id]).then(clusters => {
            console.log(`${t}==${t_comp}==${broker_id}`)
            clusters.forEach(cluster => {
                cluster.brokers.forEach(broker => {
                        postEntity(entity, broker)
                })
            })
        })
    })
    res.status(201).send({
        'message': 'Tangled'
    })
}

app.post('/notify', notify);

app.post('/notify/:broker_id', notify);

app.post('/updatePlatforms', (req, res) => {
    platforms = {}
    initializePlatforms()
});

app.post('/rules', (req, res) => {
    let rule = req.body.data
    mongoose.connect('mongodb://localhost:27017/test', { useNewUrlParser: true, useUnifiedTopology: true });
});

app.get('/clusters', (req, res) => {
    let promisse = null
    if ('entity_id' in req.query && 'entity_type' in req.query) {
        let brokers = []
        if ('brokers' in req.query) {
            brokers = req.query.brokers
        }
        promisse = cs.listClustersById(req.query.entity_type, req.query.entity_id, brokers)
    } else {
        promisse = cs.listClusters()
    }
    promisse.then((ret)=>{
        res.send(ret)
    })
})

app.post('/clusters', (req, res) => {
    (cs.createOrUpdateCluster(req.body.entity_type, req.body.entity_id, req.body.brokers))
    .then((ret)=>{
        res.send(ret)
    })
})

app.delete('/clusters', (req, res) => {
    (cs.deleteCluster(req.query.entity_type, req.query.entity_id, req.query.brokers))
    .then((ret)=>{
        res.send(ret)
    })
})

//initializePlatforms();

app.listen(3000, "0.0.0.0", function () {
    console.log("Escutando na porta 3000")
});
