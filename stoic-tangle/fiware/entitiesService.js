const request = require('request');
const fs = require('fs');

//console.log = () => {};

module.exports = class FiwareEntities {

    constructor(broker) {
        this.orion_url = broker.orion.url;
    }

    order(unordered) {
        const ordered = {};
        Object.keys(unordered).sort().forEach(function(key) {
            ordered[key] = unordered[key];
        });
        return ordered;
    }

    log(tag, obj) {
        obj = this.order(obj);
        fs.appendFile('./fiware.log', tag + " => " + JSON.stringify(obj) + '\n', function (err) {
            if (err) return console.log(err);
        });
    }

    saveEntity(entity) {
        // TODO: Gambiarra terrível pra clonar, vou trocar
        let attrs = JSON.parse(JSON.stringify(entity));
        delete attrs.id;
        delete attrs.type
        request.post({
            headers: {
                'fiware-service': 'stoic',
                'fiware-servicepath': '/',
                'content-type' : 'application/json',
                'content-length': Buffer.byteLength(JSON.stringify(attrs))},
            url: `${this.orion_url}/v2/entities/${entity.id}/attrs?type=${entity.type}`,
            body: JSON.stringify(attrs)
          }, (err, res, body) => {
            if (res.statusCode != 204) {
                request.post({ // Se não deu pra atualizar, provavelmente não existe. Criar
                    headers: {
                        'fiware-service': 'stoic',
                        'fiware-servicepath': '/',
                        'content-type' : 'application/json',
                        'content-length': Buffer.byteLength(JSON.stringify(entity))},
                    url: `${this.orion_url}/v2/entities`,
                    body: JSON.stringify(entity)
                    }, (err, res, payload) => {
                    if (res.statusCode != 201) {
                        return false; 
                    }
                    this.log("CREATED", entity)
                    console.log(`Entidade ${entity.type}:${entity.id} criada em Fiware`)
                    console.log(payload);
                    return true;
                });
                return console.log(err);
                return false;
            }
            console.log(`Entidade ${entity.type}:${entity.id} atualizada em Fiware`)
            this.log("UPDATED", entity)
            console.log(body);
            return true;
        });
    }
}