const Cloudant = require('@cloudant/cloudant');
const request = require('request');
const fs = require('fs');

//console.log = () => {};

module.exports = class WatsonEntities {

    constructor(broker) {
        this.cloudant = Cloudant({ account: broker.cloudant.user, password: broker.cloudant.password });
        this.db_entities = this.cloudant.db.use('stoic_entities')

        this.entities_rev = {};

        // Assinatura de mudanças
        this.feed = this.db_entities.follow({ since: "now" });
        this.feed.on('change', (change) => {
            console.log("MUDANÇA: " + broker.id, JSON.stringify(change));
            var _id = change.id;
            var _rev = change.changes[0].rev.split('-')[0];

            if (this.entities_rev[_id] == undefined)
                this.entities_rev[_id] = []

            if (this.entities_rev[_id].indexOf(_rev) > -1) { // Caso seja uma mudança provinda de outra plataforma
                // this.entities_rev.splice(idx, 1);
                console.log("RETURN => ", this.entities_rev[_id]);
            } else {
                console.log("NOT_RETURN => ",JSON.stringify({ "list_rev": this.entities_rev[_id], "_rev": _rev }))
                this.db_entities.get(_id).then((entity) => {
                    entity.id = entity._id.split(':')[1]
                    entity.type = entity._id.split(':')[0]
                    delete entity._id
                    delete entity._rev

                    let entities = {
                        "data": [entity]
                    }
                    request.post({
                        headers: { 'content-type': 'application/json' },
                        url: `http://localhost:3000/notify?broker_id=${broker.id}`,
                        body: JSON.stringify(entities)
                    }, (err, res, body) => {
                        if (err) { return console.log(err); }
                        console.log(body);
                    });
                }).catch((error) => {
                    console.log("Error on accessing Watson database. " + _id)
                })
            }
        });

        this.feed.follow();
    }

    // processChanges() {
    //     console.log("[processChanges()]");
    //     this.cloudant.db.changes('stoic_entities').then((body) => {
    //         console.log(body);
    //     });
    // }

    async updateEntityRev(_id, _rev) {
        if (this.entities_rev[_id] == undefined) {
            this.entities_rev[_id] = []
        } else if (this.entities_rev[_id].length > 9) {
            this.entities_rev[_id].shift();
        }
        this.entities_rev[_id].push(_rev);
        console.log("updateEntityRev(_id, _rev) => ", JSON.stringify(this.entities_rev));
        // this.processChanges();
    }

    order(unordered) {
        const ordered = {};
        Object.keys(unordered).sort().forEach(function (key) {
            ordered[key] = unordered[key];
        });
        return ordered;
    }

    async log(tag, obj) {
        obj = this.order(obj);
        fs.appendFile('./watson.log', tag + " => " + JSON.stringify(obj) + '\n', function (err) {
            if (err) return console.log(err);
        });
    }

    saveEntity(entity) {
        var bkp = JSON.parse(JSON.stringify(entity))
        let _id = `${entity.type}:${entity.id}`
        delete entity.id
        delete entity.type
        entity._id = _id

        var success = false;

        this.db_entities.get(_id).then((doc) => {
            console.log("Encontrei ")
            console.log(doc)
            entity._rev = doc._rev
            var new_rev = doc._rev.split('-')
            new_rev = `${parseInt(new_rev[0]) + 1}`
            this.feed.pause();
            this.updateEntityRev(_id, new_rev).then(() => {
                this.db_entities.insert(entity).then((body) => {
                    console.log(body)
                    console.log("UPDATED!")
                    this.log("UPDATED", bkp)
                    let _rev = body.rev.split('-')[0];
                    //this.updateEntityRev(_id, _rev);
                    success = true;
                    this.feed.resume();
                }).catch((err) => {
                    this.feed.resume();
                })
            });
        }).catch((error) => {
            this.feed.pause();
            this.updateEntityRev(_id, 1).then(() => {
                this.db_entities.insert(entity).then((body) => {
                    console.log(body)
                    console.log("INSERTED!")
                    this.log("CREATED", bkp);
                    let _rev = body.rev.split('-')[0];
                    //this.updateEntityRev(_id, _rev);
                    success = true;
                    this.feed.resume();
                }).catch((err) => {
                    this.feed.resume();
                })
            });
        })

        return success;
    }

    set name(name) {
        this._name = name.charAt(0).toUpperCase() + name.slice(1);
    }
    get name() {
        return this._name;
    }
}
