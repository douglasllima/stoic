var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');
var request = require('sync-request');

var WatsonEntities = require('./watson/entitiesService')
var FiwareEntities = require('./fiware/entitiesService')
var ClusterService = require('./service/clusters2Service')

const cs = new ClusterService("mongodb://localhost:27017", "stoic-tangle", "tangles")
async function main () {
    await cs.createOrUpdateCluster("Park","previdencia",["wiotp_broker"]);
    await cs.createOrUpdateCluster("Park","previdencia",["orion_fiware"]);
    await cs.createOrUpdateCluster("Park","previdencia",["orion_fiware","wiotp_broker"]);
}

main()