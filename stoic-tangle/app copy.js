var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');
var request = require('sync-request');

var WatsonEntities = require('./watson/entitiesService')
var FiwareEntities = require('./fiware/entitiesService')
var ClusterService = require('./service/clusters2Service')
const cs = new ClusterService("mongodb://localhost:27017", "stoic-tangle", "tangles")

//console.log = () => {};

// Load the Cloudant library.
var Cloudant = require('@cloudant/cloudant');

var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var platforms = {}

// function getEnv() {
//     var env = {}
//     env = fs.readFileSync('environment.json', 'utf8')
//     return JSON.parse(env)
// }

function getEnv() {
    let res = request('GET', 'http://localhost:8080/broker');
    let brokers = JSON.parse(res.getBody('utf8'));
    return { "brokers": brokers };
}

function initializePlatforms() {
    var env = getEnv()

    env.brokers.forEach((broker) => {
        let GenericEntities = null;

        switch (broker.type) {
            case 'watson':
                GenericEntities = WatsonEntities;
                break;

            case 'fiware':
                GenericEntities = FiwareEntities;
                break;
        }

        platforms[broker.id] = new GenericEntities(broker);
    })
}

// app.post('/env', function(req, res) {
//     fs.writeFile(env_file, JSON.stringify(req.body), function (err) {
//       if (err) return console.log(err);
//       console.log('Updating enviroment');
//     });
// })

app.post('/notify', (req, res) => {
    function order(unordered) {
        const ordered = {};
        Object.keys(unordered).sort().forEach(function (key) {
            ordered[key] = unordered[key];
        });
        return ordered;
    }

    if (!('broker_id' in req.query)) {
        res.status(400).send({
            'error': 'Without broker_id',
            'name': 'BAD_REQUEST',
            'status': 400
        })
        return
    }
    let broker_id = req.query.broker_id;
    console.log(req.body.data); // the posted data
    var env = getEnv()

    var entities = req.body.data

    for (var broker in env.brokers) {

        var sync_errors = []

        entities.forEach(entity => {
            console.log("entity => ", JSON.stringify(entity))
            if (!("id" in entity)) {
                return;
            }
            obj = order(entity);
            fs.appendFile('./entity.log', "UPDATED" + " => " + broker_id + JSON.stringify(obj) + '\n', function (err) {
                if (err) return console.log(err);
            });
            if (env.brokers[broker].id != broker_id) {
                if (platforms[broker] == undefined) {
                    let GenericEntities = null;

                    switch (env.brokers[broker].type) {
                        case 'watson':
                            GenericEntities = WatsonEntities;
                            break;

                        case 'fiware':
                            GenericEntities = FiwareEntities;
                            break;
                    }

                    platforms[broker] = new GenericEntities(env.brokers[broker]);
                }
                if (!platforms[broker].saveEntity(entity)) {
                    sync_errors.push(`Problem on accessing database. broker_id=${env.brokers[broker].id}`)
                }
            }
        });
        if (sync_errors.length > 0) {
            res.status(500).send({
                'errors': sync_errors,
                'name': 'INTERNAL_SERVER_ERROR',
                'status': 500
            });
        } else {
            res.status(204); // No content
        }
    }
});

app.post('/updatePlatforms', (req, res) => {
    platforms = {}
    initializePlatforms()
});

app.post('/rules', (req, res) => {
    let rule = req.body.data
    mongoose.connect('mongodb://localhost:27017/test', { useNewUrlParser: true, useUnifiedTopology: true });
});

app.get('/clusters', (req, res) => {
    let promisse = null
    if ('entity_id' in req.query && 'entity_type' in req.query) {
        let brokers = []
        if ('brokers' in req.query) {
            brokers = req.query.brokers
        }
        promisse = cs.listClustersById(req.query.entity_type, req.query.entity_id, brokers)
    } else {
        promisse = cs.listClusters()
    }
    promisse.then((ret)=>{
        res.send(ret)
    })
})

app.post('/clusters', (req, res) => {
    (cs.createOrUpdateCluster(req.body.entity_type, req.body.entity_id, req.body.brokers))
    .then((ret)=>{
        res.send(ret)
    })
})

app.delete('/clusters', (req, res) => {
    (cs.deleteCluster(req.query.entity_type, req.query.entity_id, req.query.brokers))
    .then((ret)=>{
        res.send(ret)
    })
})

initializePlatforms();

app.listen(3000, "0.0.0.0", function () {
    console.log("Escutando na porta 3000")
});
